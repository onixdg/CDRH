# -*- coding: UTF-8 -*-
from django import forms
from .models import Espermiograma
from django.core.exceptions import ValidationError

# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class espermio_form (forms.ModelForm):
	class Meta:
		model = Espermiograma
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'
	# Validación de campo Tiempo_licu, el timpo de licufacción no puede ser menos a 0. Y aunque por lo general la prueba no se exede de los 60 min, no se ha restringido por la construcción del parametro (puede cambiar).
	# Rrecordar que las validadiones más siomples se pueden hacer desde el mismo modelo o template 	
	def clean_Tiempo_licu(self):
		diccionario_limpio = self.cleaned_data
		tiempo_licu = diccionario_limpio.get("Tiempo_licu")
		if (tiempo_licu != None) :
			if (int(tiempo_licu) < 1):
				raise forms.ValidationError("El tiempo no puede ser menor a 1 min o mayor a los 60 min")
		return tiempo_licu
	# Validación de campo PH, el ph sólo se mueve en el rango de 0 a 14
	def clean_PH(self):
		diccionario_limpio = self.cleaned_data
		ph = diccionario_limpio.get("PH")
		if (ph != None) :
			if (int(ph) > 14 or int(ph) < 0):
				raise forms.ValidationError("El PH se mueve en un rango de 0 a 14")
		return ph
		
		