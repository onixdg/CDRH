# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from andrologia.models import *


# Create your models here.

#-----------------------------------------CLASE PACIENTE-------------------------------------------- 

#desarrollado bajo la norma Chilena " estándares de identificación de la persona atendida"
class Paciente (models.Model):
	#sexo Y Previsión son opciones numericas según la norma de salud
	SEXO = (
		(1,'hombre'),
		(2,'mujer',
		(3,'indeterminado'),
		(9,'desconocido'),
	)
	#ESTADO_CONYUGAL = (
		#(1,'Soltero (a)'),
		#(2,'Casado (a)',
		#(3,'Viudo (a)'),
		#(4,'Separado (a)'),
		#(5,'Conviviente (a)'),
		#(6,'Divorciado (a)),
		#(9,'Desconocido'),
		#)
			
	#PREVISION = (
		#(1,'FONASA'),
		#(2,'Isapre',
		#(3,'Sin previsión'),
		#(5,'CAPREDENA'),
		#(6,'DIPRECA),
		#(7,'Otra'),
		#(9,'Ignorado'),
		#)
	#CLASIFICACION_BENEFICIARIO_FONASA = (A,B,C,D)

	#LEYES_SOCIALES = (
		#(1,'Ley 18.490 Accidentes de transporte'),
		#(2,'LEY 16.744 Accidentes del trabajo y Enfermedades Profesionales',
		#(3,'Ley 16.744 Accidente Escolar'),
		#(4,'Ley 19.650/99 de Urgencia'),
		#(5,'PRAIS'),
		#(6,'Chile Solidario),
		#(7,'Chile Crece Conmigo'),
		#(8,'Otra Programa social'),
		#(9,'GES'),
		#)

	#NIVEL_INSTRUCCION = (
		#('01','Sala cuna'),
		#('02','Educación parvularia',
		#('03','Educación básica'),
		#('04','Educación media científica humanista'),
		#('05','Educación media técnico profesional'),
		#('06','Humanidades (sistema antiguo)'),
		#('08','Chile Crece Conmigo'),
		#('10','Ley 16.744 Accidente Escolar'),
		#('12','Educación universitaria'),
		#('13','Postgrado'),
		#('14','Educación especial'),
		#('15','Ninguno'),
		#)
	#ESTANDAR_ABREVIADO_NIVEL_INSTRUCCION = (
		#('1','Pre-básica'),
		#('2','Básica (incluye educación especial /diferencial)',
		#('3','Media'),
		#('4','Técnica de nivel superior'),
		#('5','Superior'),
		#('6','Ninguno'),
		#)

	Nombres = models.CharField(max_length = 50)
	Primer_apellido = models.CharField("Apellido paterno",max_length = 30)
	Segundo_apellido = models.CharField("Apellido materno",max_length = 30)
	#ojo con el validador de rut
	RUN = models.IntegerField("RUT")
	DV = models.CharField(max_length=1)
	Telefono = models.CharField(max_length= 11)
	Edad = models.IntergerField() #maximo 3 digitos
	Sexo = models.IntergerField(choices=SEXO)
	#Estado_conyugal = models.IntergerField(dígito maximo =1,choices= ESTADO_CONYUGAL)
	#Prevision = models.Charfield("previsión",digito maximo = 1, choices = PREVISION )
	#Clasificacion_Beneficiario_FONASA = models.CharField("Clasificación beneficiario FONASA", max_length =1,null =true)
	#Leyes_sociales = models.IntergerField(dígito maximo =1,choices=LEYES_SOCIALES) 
	#def definir_clasificacion_fonada(self):
		#if self.presision == 1: deberia poner la CLASIFICACION_BENEFICIARIO_FONASA = (A,B,C,D)
	#Nivel_de_instrucción= models.Charfield("Nivel de intrucción",max_length =2,null = true ,choices = NIVEL_INSTRUCCION)
	#Estandar_abreviado_nivel_de_instrucción= model.Charfield("Estándar Abreviado Nivel de Instrucción",max_length =1,null = true ,choices = ESTANDAR_ABREVIADO_NIVEL_INSTRUCCION)
	#Ocupacion = models.Charfield ("Ocupación",max_length = 4, fuente CIUO-88)
	#Actividad_economica = models.Charfield ("Actividad Económiva",max_length = 6, fuente CIIU)
	#Nacionalidad



