# -*- encoding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
	url(r'^listar_paciente/', views.listar_paciente, name='listar_paciente'),
	url(r'^crear_paciente/', views.crear_paciente, name='crear_paciente'),   


]