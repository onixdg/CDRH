# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-03 22:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('andrologia', '0002_auto_20160803_1756'),
    ]

    operations = [
        migrations.AddField(
            model_name='separacion',
            name='Volumen_post',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=3, null=True),
        ),
        migrations.AddField(
            model_name='separacion_o',
            name='Volumen_post',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=3, null=True),
        ),
    ]
