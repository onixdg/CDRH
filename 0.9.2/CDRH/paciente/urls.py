#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
	url(r'^listar_paciente/', views.listar_paciente, name='listar_paciente'),
	url(r'^crear_paciente/', views.crear_paciente, name='crear_paciente'), 

	url(r'^detalle_paciente/(?P<paciente_id>[-\w]+)/$', views.detalle_paciente, name='detalle_paciente'),
	url(r'^detalle_paciente/', views.detalle_paciente, name='detalle_paciente'),  
	url(r'^editar_paciente/(?P<paciente_id>[-\w]+)/$', views.editar_paciente, name='editar_paciente'),
	url(r'^editar_paciente/', views.editar_paciente, name='editar_paciente'),  


]