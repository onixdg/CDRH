#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes
#url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),
url(r'^crear_diagnostico_esp/(?P<espermio_id>[-\w]+)/$', views.crear_diagnostico_esp, name='crear_diagnostico_esp'),
url(r'^crear_diagnostico_esp/$', views.crear_diagnostico_esp, name='crear_diagnostico_esp'),
#url(r'^crear_inseminacion/', views.crear_inseminacion, name='crear_inseminacion'),
url(r'^crear_diagnostico_se/(?P<separacion_id>[-\w]+)/$', views.crear_diagnostico_se, name='crear_diagnostico_se'),
url(r'^crear_diagnostico_so/(?P<separacion_o_id>[-\w]+)/$', views.crear_diagnostico_so, name='crear_diagnostico_so'),
url(r'^crear_diagnostico_iiu/(?P<inseminacion_id>[-\w]+)/$', views.crear_diagnostico_iiu, name='crear_diagnostico_iiu'),
]