#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from forms import *
from diagnostico.models import *
from andrologia.models import *
from django.shortcuts import redirect
# Create your views here.


def crear_diagnostico_esp(request, espermio_id=None):
 	if (espermio_id):
 		espermio=Espermiograma.objects.get(id= espermio_id)
	if request.method =='POST':
	 	form = diag_esp_form(request.POST)
	 	if form.is_valid():
	 		espermio = form.cleaned_data.get('Espermiograma')
	 		form.save()
	 		return redirect('editar_espermio', examen_id=espermio.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_esp_form()
	 	form.fields['Espermiograma'].initial = espermio
	return render (request, 'diagnostico/crear_diagnostico_esp.html',{'form':form,'espermio':espermio})

def crear_diagnostico_se(request, separacion_id=None):
 	if (separacion_id):
 		separacion=Separacion.objects.get(id= separacion_id)
	if request.method =='POST':
	 	form = diag_se_form(request.POST)
	 	if form.is_valid():
	 		separacion = form.cleaned_data.get('Separacion')
	 		form.save()
	 		return redirect('editar_separacion', examen_id=separacion.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_se_form()
	 	form.fields['Separacion'].initial = separacion
	return render (request, 'diagnostico/crear_diagnostico_se.html',{'form':form,'separacion':separacion})

def crear_diagnostico_so(request, separacion_o_id=None):
 	if (separacion_o_id):
 		separacion_o=Separacion_o.objects.get(id= separacion_o_id)
	if request.method =='POST':
	 	form = diag_so_form(request.POST)
	 	if form.is_valid():
	 		separacion_o = form.cleaned_data.get('Separacion_o')
	 		form.save()
	 		return redirect('editar_separacion_o', examen_id=separacion_o.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_so_form()
	 	form.fields['Separacion_o'].initial = separacion_o
	return render (request, 'diagnostico/crear_diagnostico_so.html',{'form':form,'separacion_o':separacion_o})

def crear_diagnostico_iiu(request, inseminacion_id=None):
 	if (inseminacion_id):
 		inseminacion=Inseminacion.objects.get(id= inseminacion_id)
	if request.method =='POST':
	 	form = diag_iiu_form(request.POST)
	 	if form.is_valid():
	 		inseminacion = form.cleaned_data.get('Inseminacion')
	 		form.save()
	 		return redirect('editar_inseminacion', examen_id=inseminacion.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_iiu_form()
	 	form.fields['Inseminacion'].initial = inseminacion
	return render (request, 'diagnostico/crear_diagnostico_iiu.html',{'form':form,'inseminacion':inseminacion})