#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from .models import Examen
from paciente.models import Paciente
from django.core.exceptions import ValidationError


# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class hora_form (forms.ModelForm):

	class Meta:
		model = Examen
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'


	def clean_Paciente(self):
		diccionario_limpio = self.cleaned_data
		Paciente = diccionario_limpio.get("Paciente")
		if (Paciente == None or (len(str(Paciente))==0) ) :
				raise forms.ValidationError("El paciente es requerido")
		return Paciente

	def clean_Tipo(self):
		diccionario_limpio = self.cleaned_data
		Tipo = diccionario_limpio.get("Tipo")
		if (Tipo == None or (len(Tipo)==0)  ) :
				raise forms.ValidationError("El examen es requerido")
		return Tipo

	