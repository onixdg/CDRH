#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys
#encoding= utf-8
from django.shortcuts import render, redirect, get_object_or_404
from andrologia.models import *
from paciente.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import paciente_form
from django.contrib import messages
#función listar_espermiogramas que se renderiza en template listar_espermio.html, lista todos los expermiogramas existentes
def listar_paciente(request):
	paciente = Paciente.objects.all().order_by('Primer_apellido').order_by('Segundo_apellido').order_by('Nombres')
	return render (request, "paciente/listar_paciente.html", {'paciente': paciente})

def listar_paciente_a(request):
	paciente = Paciente.objects.all().order_by('Primer_apellido').order_by('Segundo_apellido').order_by('Nombres')
	return render (request, "paciente/listar_paciente_a.html", {'paciente': paciente})

# Create your views here.
def crear_paciente(request):
 	
	 	if request.method =='POST':
	 		form = paciente_form(request.POST)
	 		
	 		if form.is_valid():
	 			conyuge = form.cleaned_data.get('Conyuge')
	 			form.save()
	 			messages.add_message(request, messages.SUCCESS, 'El paciente se ha creado de forma exitosa.')
	 			if (conyuge):
	 				Conyuge = Paciente.objects.get(id = conyuge.id)
	 				Pareja =  Paciente.objects.last()
	 				conyuge.Conyuge = Pareja
	 				conyuge.save()
	 				
	 			return redirect('listar_paciente')
	 	else:
	 		
	 		form= paciente_form()
	 	return render (request, 'paciente/crear_paciente.html',{'form':form})

def detalle_paciente (request, paciente_id):
	paciente = get_object_or_404(Paciente,pk=paciente_id)
	return render (request, 'paciente/detalle_paciente.html',{'paciente':paciente})

def editar_paciente(request,paciente_id = None):
	paciente = get_object_or_404(Paciente,pk=paciente_id)
	if (paciente.Conyuge):
 			request.session["conini"] = paciente.Conyuge.id
 	else:
 			request.session["conini"] = None
	if request.method =='POST':
	 		form = paciente_form(request.POST, instance = paciente)
	 		
	 		if form.is_valid():
	 			conyuge = form.cleaned_data.get('Conyuge')
	 			form.save()

	 			if(request.session["conini"]):
		 			P_id = request.session["conini"]
		 			Con_ini = Paciente.objects.get(id = P_id)
		 		else:
		 			Con_ini = None
	 			if (Con_ini != conyuge):
	 			 	
	 				if (conyuge==None ):
	 					con_ori = Paciente.objects.get(id = Con_ini.id)
	 					con_ori.Conyuge = None
	 					con_ori.save()
	 					

	 				else:
		 				if (Con_ini):
		 					con_ori = Paciente.objects.get(id = Con_ini.id)
		 					con_ori.Conyuge = None
		 					con_ori.save()
		 					con_ori = Paciente.objects.get(id = conyuge.id)
		 					con_ori.Conyuge = paciente
		 					con_ori.save()
		 				
		 				else:
		 					con_ori = Paciente.objects.get(id = conyuge.id)
		 					con_ori.Conyuge = paciente
		 					con_ori.save()

		 		messages.add_message(request, messages.SUCCESS, ('El paciente %(value)s se ha editado de forma exitosa.') % {'value': paciente})			


	 		return redirect('listar_paciente')
 
 	else:

 		form = paciente_form(instance = paciente)
 		
	return render (request, 'paciente/editar_paciente.html',{'form':form,'paciente':paciente})

def editar_paciente_a(request,paciente_id = None):
	paciente = get_object_or_404(Paciente,pk=paciente_id)
	if (paciente.Conyuge):
 			request.session["conini"] = paciente.Conyuge.id
 	else:
 			request.session["conini"] = None
	if request.method =='POST':
	 		form = paciente_form(request.POST, instance = paciente)
	 		
	 		if form.is_valid():
	 			conyuge = form.cleaned_data.get('Conyuge')
	 			form.save()

	 			if(request.session["conini"]):
		 			P_id = request.session["conini"]
		 			Con_ini = Paciente.objects.get(id = P_id)
		 		else:
		 			Con_ini = None
	 			if (Con_ini != conyuge):
	 			 	
	 				if (conyuge==None ):
	 					con_ori = Paciente.objects.get(id = Con_ini.id)
	 					con_ori.Conyuge = None
	 					con_ori.save()
	 					

	 				else:
		 				if (Con_ini):
		 					con_ori = Paciente.objects.get(id = Con_ini.id)
		 					con_ori.Conyuge = None
		 					con_ori.save()
		 					con_ori = Paciente.objects.get(id = conyuge.id)
		 					con_ori.Conyuge = paciente
		 					con_ori.save()
		 					
		 				else:
		 					con_ori = Paciente.objects.get(id = conyuge.id)
		 					con_ori.Conyuge = paciente
		 					con_ori.save()

		 		messages.add_message(request, messages.SUCCESS, ('El paciente %(value)s se ha editado de forma exitosa.') % {'value': paciente})			


	 		return redirect('listar_paciente_a')
 
 	else:

 		form = paciente_form(instance = paciente)
 		
	return render (request, 'paciente/editar_paciente.html',{'form':form,'paciente':paciente})
