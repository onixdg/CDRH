
    function multiplicar(){
      m1 = document.getElementById("multiplicando").value;
      m1= m1.replace(",", "."); 
     
      m2 = document.getElementById("multiplicador").value;
      m2= m2.replace(",", "."); 
      
      r = m1*m2;
      r= r.toFixed(2)
      document.getElementById("resultado").value = r;
    }

    function multiplicar_tem(){
      m1 = document.getElementById("resultado").value;
      m1= m1.replace(",", "."); 
    
      m2 = document.getElementById("progresivos").value;
      m2= m2.replace(",", "."); 
   	  m2= m2/100;
      r = m1*m2;
      
      r= r.toFixed(2)
      document.getElementById("tem").value = r;
    }

    function multiplicar_post(){
      m1 = document.getElementById("multiplicando_post").value;
      m1= m1.replace(",", "."); 
     
      m2 = document.getElementById("multiplicador_post").value;
      m2= m2.replace(",", "."); 
     
      r = m1*m2;
      r= r.toFixed(2)
      document.getElementById("resultado_post").value = r;
    }

    function multiplicar_tem_post(){
      m1 = document.getElementById("resultado_post").value;
      m1= m1.replace(",", "."); 
    
      m2 = document.getElementById("progresivos_post").value;
      m2= m2.replace(",", "."); 
      m2= m2/100;
      r = m1*m2;
      
      r= r.toFixed(2);
      document.getElementById("tem_post").value = r;
    }

    function multiplicar_post_s(){
      m1 = document.getElementById("concentracion_post").value;
      m1= m1.replace(",", "."); 
     
      m2 = document.getElementById("volumen_post").value;
      m2= m2.replace(",", "."); 
     
      m3 = document.getElementById("progresivos_post").value;
      m3= m3.replace(",", "."); 
      m3= m3/100;
      r = m1*m2*m3;
      r= r.toFixed(2)
      document.getElementById("tem_post").value = r;
    }


    function suma(){
      m1 = document.getElementById("progresivos").value;
      m2 = document.getElementById("no_progresivos").value;
      m3 = document.getElementById("inmoviles").value;
      s = parseInt(m1)+parseInt(m2)+parseInt(m3);

      if (m1 !="" && m2 !="" && m3 =="") {
        suma= parseInt(m1)+parseInt(m2)
        sum = 100 - suma 
        document.getElementById("inmoviles").value = sum;

      }

      if (m1 !="" && m2 =="" && m3 !="") {
        suma= parseInt(m1)+parseInt(m3)
        sum = 100 - suma 
        document.getElementById("no_progresivos").value = sum;

      }


      if (s != 100 && m1 !="" && m2 !="" && m3 !="") {
        alert("Recuerde que entre la suma de los porcentajes de espermatozoides progresivos, no progresivos e inmoviles se debe generar un total de 100% correspondiente a la totalidad de la muestra.");
        // document.getElementById("progresivos").value = "";
        document.getElementById("no_progresivos").value = "";
        document.getElementById("inmoviles").value = "";
      }
    }
      
    function suma_n(){
      m1 = document.getElementById("normales").value;
      m2 = document.getElementById("anormales").value;
      s = parseInt(m1)+parseInt(m2);

      if (m1 !="" && m2 =="") {
        suma = 100 - parseInt(m1)
        document.getElementById("anormales").value = suma;

      }

      if (m1 =="" && m2 !="") {
        suma = 100 - parseInt(m2)
        document.getElementById("normales").value = suma;

      }


      if (s != 100 && m1 !="" && m2 !="" ) {
        alert("Recuerde que entre la suma de los porcentajes de espermatozoides normales y anormales se debe generar un total de 100% correspondiente a la totalidad de la muestra.");
        document.getElementById("normales").value = "";
        document.getElementById("anormales").value = "";
      } 
    }

    function suma_post(){
      m1 = document.getElementById("progresivos_post").value;
      m2 = document.getElementById("no_progresivos_post").value;
      m3 = document.getElementById("inmoviles_post").value;
      s = parseInt(m1)+parseInt(m2)+parseInt(m3);

      if (m1 !="" && m2 !="" && m3 =="") {
        suma= parseInt(m1)+parseInt(m2)
        sum = 100 - suma 
        document.getElementById("inmoviles_post").value = sum;

      }

      if (m1 !="" && m2 =="" && m3 !="") {
        suma= parseInt(m1)+parseInt(m3)
        sum = 100 - suma 
        document.getElementById("no_progresivos_post").value = sum;

      }

      if (s != 100 && m1 !="" && m2 !="" && m3 !="") {
        alert("Recuerde que entre la suma de los porcentajes de espermatozoides progresivos, no progresivos e inmoviles se debe generar un total de 100% correspondiente a la totalidad de la muestra.");
        document.getElementById("no_progresivos").value = "";
        document.getElementById("inmoviles").value = "";
      }
    }