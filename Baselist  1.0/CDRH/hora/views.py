#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from hora.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import hora_form
from django.contrib import messages
from datetime import *
# Create your views here.

# Create your views here.
def crear_hora(request):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 	
	 	if request.method =='POST':
	 		form = hora_form(request.POST)
	 		if form.is_valid():
	 			form.save()
	 			tipo = form.cleaned_data.get('Tipo')
	 			paciente = form.cleaned_data.get('Paciente')

	 			if (tipo == 'es'):
	 				tipo = "espermiograma"
	 			elif (tipo =='se'):
	 				tipo = "separación espermática"
	 			elif (tipo =='so'):
	 				tipo = "separación espermática en orina"
	 			elif (tipo =='iiu'):
	 				tipo = "inseminación intrauterina"
	 			

	 			messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})
	 			
				#messages.error(request, 'We could not process your request at this time.')
				examen = Examen.objects.last()
	 			return redirect('crear_antecedentes_r_s',examen.id)
	 	else:
	 		
	 		form= hora_form()
	 	return render (request, 'hora/crear_hora.html',{'form':form})

def crear_hora_a(request):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 	
	 	if request.method =='POST':
	 		form = hora_form(request.POST)
	 		if form.is_valid():
	 			form.save()
	 			tipo = form.cleaned_data.get('Tipo')
	 			paciente = form.cleaned_data.get('Paciente')

	 			if (tipo == 'es'):
	 				tipo = "espermiograma"
	 			elif (tipo =='se'):
	 				tipo = "separación espermática"
	 			elif (tipo =='so'):
	 				tipo = "separación espermática en orina"
	 			elif (tipo =='iiu'):
	 				tipo = "inseminación intrauterina"
	 			

	 			messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})
	 			
	 			examen = Examen.objects.last()
	 			return redirect('crear_antecedentes_r_a',examen.id)
	 	else:
	 		
	 		form= hora_form()
	 	return render (request, 'hora/crear_hora_a.html',{'form':form})

def listar_examen(request):
	examen = Examen.objects.all().order_by('-Fecha')
	hoy = date.today()
	ant = hoy.month -1
	actual = Examen.objects.filter(Fecha__month = hoy.month , Fecha__year = hoy.year, Est_hora ='a').count()
	anterior =Examen.objects.filter(Fecha__month = ant , Fecha__year = hoy.year, Est_hora ='a').count()
	anual = Examen.objects.filter(Fecha__year = hoy.year, Est_hora ='a').count()
	return render (request, "hora/listar_examen.html", {'examen': examen,'actual':actual,'anterior':anterior,'anual':anual})