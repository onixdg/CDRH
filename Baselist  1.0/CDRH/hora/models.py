#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from paciente.models import Paciente as pac

#------------------------------------------CLASE EXAMEN-----------------------------------------
class Examen(models.Model):
	TIPO = (
		('es' , 'Espermiograma'),
		('so' , 'Separación espermática en orina'),
		('se' , 'Separación espermática'),
		('iiu' , 'Inseminación intra uterina'),
		#('ce' , 'Criopreservación de espermios'),
		#('sa' , 'Separación de ADN'),
		)
	EST_INFORME = (
		('i' , 'Incompleto'),
		('c' , 'Completo'),
		)	
	EST_HORA = (
		('a' , 'Atendido'),
		('e' , 'En espera'),
		('n' , 'No se presenta'),
		)
	Paciente = models.ForeignKey(pac)	
	Tipo = models.CharField(max_length = 3,choices = TIPO)
	Fecha = models.DateTimeField(auto_now_add=True)
	Est_informe= models.CharField(max_length = 3,choices = EST_INFORME, default ="i")
	Est_hora= models.CharField(max_length = 3,choices = EST_HORA, default ="e")

	