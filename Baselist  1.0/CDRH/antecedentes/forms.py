#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from .models import *
from paciente.models import Paciente
from hora.models import Examen
from django.core.exceptions import ValidationError


# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class habitos_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_habitos
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'


class medicos_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_medicos
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class testiculares_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_testiculares
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class recientes_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_recientes
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

	def clean_Medico_tratante(self):
		diccionario_limpio = self.cleaned_data
		Medico_tratante = diccionario_limpio.get("Medico_tratante")
		if (Medico_tratante == None or (len(Medico_tratante)==0) ) :
				raise forms.ValidationError("El Médico tratante es requeridos")
		return Medico_tratante