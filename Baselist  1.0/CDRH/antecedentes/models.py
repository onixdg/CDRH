#!/usr/bin/env python
# -*- coding: utf-8 -*--
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from paciente.models import Paciente as pac
from hora.models import Examen as exm
from django.utils import timezone


# Create your models here.
class Antecedentes_habitos(models.Model):
	Paciente = models.ForeignKey(pac)
	TABACO = (

		('n','No'),
		('o','Ocasional'),
		('d','Diario'),
		('s','Suspendido'),
		
		)
	ALCOHOL = (

		('n','No'),
		('o','Ocasional'),
		('d','Diario'),
		('s','Suspendido'),
		
		)

	MARIHUANA = (

		('n','No'),
		('o','Ocasional'),
		('d','Diario'),
		('s','Suspendido'),
		
		)

	COCAINA = (

		('n','No'),
		('o','Ocasional'),
		('d','Diario'),
		('s','Suspendido'),
		
		)

	PASTA = (

		('n','No'),
		('o','Ocasional'),
		('d','Diario'),
		('s','Suspendido'),
		
		)

	#Fecha_cambio = models.DateTimeField(auto_now_add=True)
	Tabaco = models.CharField("Tabaco",max_length = 1,choices = TABACO)
	Alcohol = models.CharField("Alcohol",max_length = 1,choices = ALCOHOL)
	Marihuana = models.CharField("Marihuana",max_length = 1,choices = MARIHUANA)
	Cocaina = models.CharField("Cocaina",max_length = 1,choices = COCAINA)
	Pasta = models.CharField("Pasta",max_length = 1,choices = PASTA)
	Drogas = models.CharField("Otras",max_length = 100, blank = True,null = True)
	Quimicos = models.BooleanField()
	Radiacion = models.BooleanField()
	Hijos = models.BooleanField()
	
class Antecedentes_medicos(models.Model):
	Paciente = models.ForeignKey(pac)
	#Fecha_cambio = models.DateTimeField(auto_now_add=True)
	Paperas = models.BooleanField()
	Fiebre = models.BooleanField()
	Fiebre_detalle = models.CharField("Cuándo",max_length = 50, blank = True,null = True)
	Antibiotico = models.BooleanField()
	Antibiotico_detalle = models.CharField("Cuándo",max_length = 50, blank = True,null = True)
	Enfermedades_cronicas = models.BooleanField("Medicamento enf. crónicas")
	Enfermedades_detalle = models.CharField("Cuáles",max_length = 50, blank = True,null = True)
	
class Antecedentes_testiculares(models.Model):
	Paciente = models.ForeignKey(pac)
	#Fecha_cambio = models.DateTimeField(auto_now_add=True)
	Varicocele = models.BooleanField()
	Hidrocele = models.BooleanField()
	Criptorquidia = models.BooleanField()
	Tumores = models.BooleanField()
	Golpes_testiculares = models.CharField(max_length = 50, blank = True,null = True)

class Antecedentes_recientes(models.Model):
	Examen = models.ForeignKey(exm)
	Abstinencia = models.IntegerField("Días de abstinencia",blank=True,null = True)
	Medico_tratante = models.CharField("Médico tratante",max_length = 50, blank = True,null = True)
	