
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from hora.models import *
from antecedentes.models import *
from diagnostico.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from django.shortcuts import get_object_or_404
from io import BytesIO
import time
import datetime
import io
from reportlab.lib.pagesizes import legal
from reportlab.pdfgen import canvas
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.platypus import Table
from reportlab.lib import colors
from django.contrib import messages

#def informe_espermiograma_prueba(request, espermio_id):
    # Create the HttpResponse object with the appropriate PDF headers.
    #espermiograma = get_object_or_404(Espermiograma,pk=espermio_id)
    #nombre =  espermiograma.Info_examen.Paciente.Nombres.replace(' ', '_')
    #print "Genero el PDF"
    #response = HttpResponse(content_type='application/pdf')
    #pdf_name = "espermiograma_" + nombre + ".pdf"  # llamado clientes
    ## la linea 26 es por si deseas descargar el pdf a tu computadora
    #response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
    #buff = BytesIO()
    #doc = SimpleDocTemplate(buff,
                            #pagesize=letter,
                            #rightMargin=40,
                            #leftMargin=40,
                            #topMargin=60,
                            #bottomMargin=18,
                            #)
    #informe = []
    #styles = getSampleStyleSheet()
    #header = Paragraph("Espermiograma", styles['Heading1'])
    #informe.append(header)
    #informe.append(Spacer(1,12))
    #headings = ('Nombre', 'Email', 'Edad', 'Dirección')
    #allclientes = [(p.Nombres, p.Email, p.Edad, p.Direccion) for p in Paciente.objects.all()]
    #print allclientes

    #t = Table([headings] + allclientes)
    #t.setStyle(TableStyle(
        #[
            #('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
            #('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
            #('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
        #]
    #))
    #informe.append(t)
    #doc.build(informe)
    #response.write(buff.getvalue())
    #buff.close()
    #return response
def informe_espermiograma(request, espermio_id): 
    espermiograma = get_object_or_404(Espermiograma,pk=espermio_id) 
    paciente =  (espermiograma.Info_examen.Paciente.Nombres.replace(' ', '_')) +"_"+espermiograma.Info_examen.Paciente.Primer_apellido 
    nombre = "esp_" + paciente  + ".pdf" 
    response = HttpResponse(content_type='application/pdf') 
    response['Content-Disposition'] = 'attachment; filename=%s' % nombre 
    informe = canvas.Canvas(response, pagesize= legal) 
    informe.drawImage('static/img/uv.jpeg',30,950,90,45) 
    informe.setLineWidth(.3) 
    informe.setFont('Helvetica-Bold', 12) 
    informe.drawString(220,950,'INFORME ESPERMIOGRAMA') 
    informe.setFont('Helvetica', 8) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,920,'Fecha:') 
    informe.drawString(80,920,'%s' % espermiograma.Info_examen.Fecha.strftime('%d-%m-%Y')) 
    
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,900,'Nombre:') 
    nombre = espermiograma.Info_examen.Paciente.Nombres +" "+espermiograma.Info_examen.Paciente.Primer_apellido+" "+espermiograma.Info_examen.Paciente.Segundo_apellido 
    informe.drawString(80,900,'%s' % nombre) 
    
    if (espermiograma.Info_examen.Paciente.RUN):
        informe.drawString(320,900,'Rut:') 
        rut = str(espermiograma.Info_examen.Paciente.RUN) +"-"+espermiograma.Info_examen.Paciente.DV 
        informe.drawString(370,900,'%s' % rut) 
    else:
        informe.drawString(320,900,'DI:') 
        di = str(espermiograma.Info_examen.Paciente.DI) 
        informe.drawString(370,900,'%s' % di) 


    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica', 8) 
    informe.drawString(30,880,'Edad:') 
    informe.drawString(80,880,'%s' % espermiograma.Info_examen.Paciente.Edad) 
    
    if (espermiograma.Info_examen.Paciente.Conyuge):
        informe.drawString(320,880,'Cónyuge:') 
        conyuge = espermiograma.Info_examen.Paciente.Conyuge.Nombres +" "+espermiograma.Info_examen.Paciente.Conyuge.Primer_apellido+" "+espermiograma.Info_examen.Paciente.Conyuge.Segundo_apellido 
        informe.drawString(370,880,'%s' % conyuge) 

    #------------------------------------------------------------------------------------------------- 
    recientes = Antecedentes_recientes.objects.filter(Examen= espermiograma.Info_examen)
    if (recientes):
        informe.drawString(30,860,'Médico:') 
        informe.drawString(80,860,'%s' % recientes[0].Medico_tratante)
    
    if (espermiograma.Info_examen.Paciente.Telefono):
        informe.drawString(320,860,'Telefono:') 
        telefono = str(espermiograma.Info_examen.Paciente.Codigo) +"-"+str(espermiograma.Info_examen.Paciente.Telefono) 
        informe.drawString(370,860,'%s' % telefono) 

    #------------------------------------------------------------------------------------------------- 
    antecedentes_h = Antecedentes_habitos.objects.filter(Paciente= espermiograma.Info_examen.Paciente)
    if (antecedentes_h):
        habito = "Tabaco: "+antecedentes_h[0].get_Tabaco_display()+", Alcohol: "+ antecedentes_h[0].get_Alcohol_display()
        habito2 = " "
        habito3 = " "
        if (antecedentes_h[0].Marihuana):
            habito = str(habito) + ", Marihuana: "+ str(antecedentes_h[0].get_Marihuana_display())
        if (antecedentes_h[0].Pasta):
            habito2 = "Pasta: "+ str(antecedentes_h[0].get_Pasta_display())
        if (antecedentes_h[0].Cocaina):
            habito2 =str(habito2) + ", Cocaína: "+ str(antecedentes_h[0].get_Cocaina_display())
        if (antecedentes_h[0].Drogas):
            habito2 = str(habito2) +", Otras drogas:" + str(antecedentes_h[0].Drogas)
        if (antecedentes_h[0].Radiacion):
            habito3 = "Radiación: Sí ,"
        if (antecedentes_h[0].Quimicos):
            habito3 = str(habito3) + "Químicos: Sí."

        informe.drawString(30,840,'Hábitos:') 
        informe.drawString(80,840,'%s' % habito) 
        informe.drawString(80,830,'%s' % habito2)

        informe.drawString(30,820,'Exposición:') 
        informe.drawString(80,820,'%s' % habito3)
   
 
    informe.drawString(320,840,'Hora:') 

    informe.drawString(370,840,'%s' % espermiograma.Info_examen.Fecha.strftime('%X')) 

    #------------------------------------------------------------------------------------------------- 
    if (recientes):
        informe.drawString(30,800,'Días de abstinencia:') 
        informe.drawString(110,800,'%s' % recientes[0].Abstinencia)

    antecedentes_t = Antecedentes_testiculares.objects.filter(Paciente= espermiograma.Info_examen.Paciente)
    if (antecedentes_t):
        testicular= " "
        if (antecedentes_t[0].Varicocele):
            testicular = "Varicocele ,"
        if (antecedentes_t[0].Hidrocele):
            testicular = str(testicular) +"Hidrocele ,"
        if (antecedentes_t[0].Criptorquidia):
            testicular = str(testicular) +"Criptorquidia ,"
        if (antecedentes_t[0].Tumores):
            testicular = str(testicular) +"Tumores ,"
        if (antecedentes_t[0].Golpes_testiculares):
            testicular = str(testicular) +"Golpes testiculares."
        informe.drawString(320,820,'Otros:') 
        informe.drawString(370,820,'%s' % testicular) 

    #-----------------------------EXAME MACROSCOPICO------------------------------------ 
    informe.line(30,790,580,790) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,770,'1.Examen macroscópico') 
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,760,'Valor ref') 
    informe.drawString(470,760,'Valor ref') 
    informe.setFont('Helvetica', 8) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,740,'Tiempo Licuefacción:') 
    informe.drawString(120,740,'%s' % espermiograma.Tiempo_licu) 
    
    informe.drawString(280,740,'PH:') 
    informe.drawString(380,740,'%s' % espermiograma.PH) 
    informe.drawString(470,740,'(>7,2)') 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,720,'Volumen:') 
    informe.drawString(120,720,'%s' % espermiograma.Volumen +" "+"ml") 
    informe.drawString(210,720,'(>1,5 ml)') 
    
    informe.drawString(280,720,'Viscosidad:') 
    informe.drawString(380,720,'%s' % espermiograma.Viscosidad) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,700,'Aspecto:') 
    informe.drawString(120,700,'%s' % espermiograma.get_Aspecto_display()) 
     
    informe.drawString(280,700,'C. gelatinosos:') 
    informe.drawString(380,700,'%s' % espermiograma.Cuerpos_gelatinosos) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,680,'Color:') 
    informe.drawString(120,680,'%s' % espermiograma.get_Color_display()) 
     
    informe.drawString(280,680,'C.en suspención:') 
    informe.drawString(380,680,'%s' % espermiograma.Cuerpos_en_suspen) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(280,660,'C. mucosos:') 
    informe.drawString(380,660,'%s' % espermiograma.Cuerpos_mucosos) 

    #----------------------------------EXAME MICROSCÓPICO--------------------------------------------- 
    informe.line(30,650,580,650) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,630,'2.Examen microscópico') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,620,'Valor ref') 
    informe.drawString(470,620,'Valor ref') 
    informe.setFont('Helvetica-Bold', 9) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,600,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(193,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(180,600,'x10 /ml') 
    informe.drawString(160,600,'%s' % espermiograma.Concentracion) 
    
    informe.setFont('Helvetica', 5) 
    informe.drawString(248,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(235,600,'x10 /ml)') 
    informe.drawString(210,600,'(>15') 
     
    informe.drawString(280,600,'Células redondas:')
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,600,'x10 /ml')  
    informe.drawString(380,600,'%s' % espermiograma.Celulas_redondas) 

    informe.setFont('Helvetica', 5) 
    informe.drawString(500,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(487,600,'x10 /ml)') 
    informe.drawString(470,600,'(<1') 
    
    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,580,'Recuento total:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(158,585,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(145,580,'x10') 
    informe.drawString(120,580,'%s' % espermiograma.Total) 
    
    informe.setFont('Helvetica', 5) 
    informe.drawString(243,585,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(230,580,'x10  )') 
    informe.drawString(210,580,'(>39') 
     
    informe.drawString(280,580,'Detritus:') 
    informe.drawString(380,580,'%s' % espermiograma.get_Detritus_display()) 
 
    #------------------------------------------------------------------------------------------------ 
    informe.drawString(280,560,'Células escamosas:') 
    informe.drawString(380,560,'%s' % espermiograma.Celulas_escamosas) 

    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,540,'Vitalidad:') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(120,540,'%s' % espermiograma.Vitalidad+" "+"%") 
    informe.drawString(210,540,'(>58%)') 

    informe.drawString(280,540,'Aglutinación:') 
    informe.drawString(380,540,'%s' % espermiograma.get_Aglutinacion_display()) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,520,'Pseudoaglutinación:') 
    informe.drawString(380,520,'%s' % espermiograma.Pseudoaglutinacion) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,500,'Cristales:') 
    informe.drawString(380,500,'%s' % espermiograma.Cristales) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,480,'Mucus:') 
    informe.drawString(380,480,'%s' % espermiograma.Mucus) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,460,'Bacterias:') 
    informe.drawString(380,460,'%s' % espermiograma.Bacterias) 

     #----------------------------------MOTILIDAD--------------------------------------------- 
    informe.line(30,450,580,450) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,430,'2.Motilidad') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,420,'Valor ref') 
    informe.setFont('Helvetica', 9) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,400,'Progresivos:') 
    informe.drawString(120,400,'%s' % espermiograma.Progresivos +" "+"%") 
    informe.drawString(210,400,'(>32%)') 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,380,'No progresivos:') 
    informe.drawString(120,380,'%s' % espermiograma.No_progresivos +" "+"%") 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,360,'Inmóviles:') 
    informe.drawString(120,360,'%s' % espermiograma.Inmoviles +" "+"%") 

    #-----------------------------MORFOLOGÍA------------------------------------ 
    informe.line(30,350,580,350) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,330,'4.Morfología') 
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,320,'Valor ref') 
    informe.drawString(470,320,'Valor ref') 
    informe.setFont('Helvetica', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,300,'Normales:') 
    informe.drawString(120,300,'%s' % espermiograma.Normales +" "+"%") 
    informe.drawString(210,300,'(>4%)') 
     
    informe.drawString(280,300,'Defecto cabeza:') 
    informe.drawString(380,300,'%s' % espermiograma.Defecto_cabeza) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,280,'Anormales:') 
    informe.drawString(120,280,'%s' % espermiograma.Anormales+" "+"%") 
     
    informe.drawString(280,280,'Defecto pieza 1/2:') 
    informe.drawString(380,280,'%s' % espermiograma.Defecto_pieza)  
     
    #------------------------------------------------------------------------------------------------- 

    informe.drawString(280,260,'Defecto cola:') 
    informe.drawString(380,260,'%s' % espermiograma.Defecto_cola) 


    #-----------------------------------------------TEM----------------------------------------------- 
    informe.line(30,250,580,250) 
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,235,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,230,'x10') 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,230,'TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) EN LA MUESTRA SEMINAL: %s' %  espermiograma.TEM) 
    
    #-----------------------------------------------DIAGNOSTICO----------------------------------------------- 
    informe.line(30,220,580,220) 
    informe.drawString(30,200,'Diágnostico:')
    informe.drawString(150,200,'Observación:')
    informe.setFont('Helvetica', 8)
    j= 180
    for i in espermiograma.diagnostico_esp_set.all() :
        informe.drawString(30,j,'%s' % i.get_Diagnostico_display())
        if (i.Observacion):
            informe.drawString(150,j,'%s' % i.Observacion)
        j = j - 20

    #-----------------------------------------------COMENTARIO----------------------------------------------- 
    informe.line(30,j,580,j)
    informe.setFont('Helvetica',8)
    j=j-20 
    informe.drawString(30,j,'Valeria Andrea Concha Stark')
    informe.drawString(30,j-20,'Tecnólogo Médico - Laboratorio de Andrología')


    informe.showPage() 
    informe.save() 
    return response


def informe_separacion(request, separacion_id): 
    separacion = get_object_or_404(Separacion,pk=separacion_id) 
    paciente =  (separacion.Info_examen.Paciente.Nombres.replace(' ', '_')) +"_"+separacion.Info_examen.Paciente.Primer_apellido 
    nombre = "se_" + paciente +".pdf" 
    response = HttpResponse(content_type='application/pdf') 
    response['Content-Disposition'] = 'attachment; filename=%s' % nombre 
    informe = canvas.Canvas(response, pagesize= legal) 
    informe.drawImage('static/img/uv.jpeg',30,950,90,45) 
    informe.setLineWidth(.3) 
    informe.setFont('Helvetica-Bold', 12) 
    informe.drawString(180,950,'INFORME SEPARACIÓN ESPERMÁTICA') 
    informe.setFont('Helvetica', 8) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,920,'Fecha:') 
    informe.drawString(80,920,'%s' % separacion.Info_examen.Fecha.strftime('%d-%m-%Y')) 
    
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,900,'Nombre:') 
    nombre = separacion.Info_examen.Paciente.Nombres +" "+separacion.Info_examen.Paciente.Primer_apellido+" "+separacion.Info_examen.Paciente.Segundo_apellido 
    informe.drawString(80,900,'%s' % nombre) 
    
    if (separacion.Info_examen.Paciente.RUN):
        informe.drawString(320,900,'Rut:') 
        rut = str(separacion.Info_examen.Paciente.RUN) +"-"+separacion.Info_examen.Paciente.DV 
        informe.drawString(370,900,'%s' % rut) 
    else:
        informe.drawString(320,900,'DI:') 
        di = str(separacion.Info_examen.Paciente.DI) 
        informe.drawString(370,900,'%s' % di) 


    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica', 8) 
    informe.drawString(30,880,'Edad:') 
    informe.drawString(80,880,'%s' % separacion.Info_examen.Paciente.Edad) 
    
    if (separacion.Info_examen.Paciente.Conyuge):
        informe.drawString(320,880,'Cónyuge:') 
        conyuge = separacion.Info_examen.Paciente.Conyuge.Nombres +" "+separacion.Info_examen.Paciente.Conyuge.Primer_apellido+" "+separacion.Info_examen.Paciente.Conyuge.Segundo_apellido 
        informe.drawString(370,880,'%s' % conyuge) 

    #------------------------------------------------------------------------------------------------- 
    recientes = Antecedentes_recientes.objects.filter(Examen= separacion.Info_examen)
    if (recientes):
        informe.drawString(30,860,'Médico:') 
        informe.drawString(80,860,'%s' % recientes[0].Medico_tratante)
    
    if (separacion.Info_examen.Paciente.Telefono):
        informe.drawString(320,860,'Telefono:') 
        telefono = str(separacion.Info_examen.Paciente.Codigo) +"-"+str(separacion.Info_examen.Paciente.Telefono) 
        informe.drawString(370,860,'%s' % telefono) 

    #------------------------------------------------------------------------------------------------- 
    antecedentes_h = Antecedentes_habitos.objects.filter(Paciente= separacion.Info_examen.Paciente)
    if (antecedentes_h):
        habito = "Tabaco: "+antecedentes_h[0].get_Tabaco_display()+", Alcohol: "+ antecedentes_h[0].get_Alcohol_display()
        habito2 = " "
        habito3 = " "
        if (antecedentes_h[0].Marihuana):
            habito = str(habito) + ", Marihuana: "+ str(antecedentes_h[0].get_Marihuana_display())
        if (antecedentes_h[0].Pasta):
            habito2 = "Pasta: "+ str(antecedentes_h[0].get_Pasta_display())
        if (antecedentes_h[0].Cocaina):
            habito2 =str(habito2) + ", Cocaína: "+ str(antecedentes_h[0].get_Cocaina_display())
        if (antecedentes_h[0].Drogas):
            habito2 = str(habito2) +", Otras drogas:" + str(antecedentes_h[0].Drogas)
        if (antecedentes_h[0].Radiacion):
            habito3 = "Radiación: Sí ,"
        if (antecedentes_h[0].Quimicos):
            habito3 = str(habito3) + "Químicos: Sí."

        informe.drawString(30,840,'Hábitos:') 
        informe.drawString(80,840,'%s' % habito) 
        informe.drawString(80,830,'%s' % habito2)

        informe.drawString(30,820,'Exposición:') 
        informe.drawString(80,820,'%s' % habito3)
   
 
    informe.drawString(320,840,'Hora:') 

    informe.drawString(370,840,'%s' % separacion.Info_examen.Fecha.strftime('%X')) 
    #------------------------------------------------------------------------------------------------- 
    if (recientes):   
        informe.drawString(30,800,'Días de abstinencia:') 
        informe.drawString(110,800,'%s' % recientes[0].Abstinencia)
        
    antecedentes_t = Antecedentes_testiculares.objects.filter(Paciente= separacion.Info_examen.Paciente)
    if (antecedentes_t):
        testicular= " "
        if (antecedentes_t[0].Varicocele):
            testicular = "Varicocele ,"
        if (antecedentes_t[0].Hidrocele):
            testicular = str(testicular) +"Hidrocele ,"
        if (antecedentes_t[0].Criptorquidia):
            testicular = str(testicular) +"Criptorquidia ,"
        if (antecedentes_t[0].Tumores):
            testicular = str(testicular) +"Tumores ,"
        if (antecedentes_t[0].Golpes_testiculares):
            testicular = str(testicular) +"Golpes testiculares."
        informe.drawString(320,820,'Otros:') 
        informe.drawString(370,820,'%s' % testicular) 


    #-----------------------------EXAME MACROSCOPICO------------------------------------ 
    informe.line(30,790,580,790) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,770,'1.Examen macroscópico') 
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,760,'Valor ref') 
    informe.drawString(470,760,'Valor ref') 
    informe.setFont('Helvetica', 8) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,740,'Tiempo Licuefacción:') 
    informe.drawString(120,740,'%s' % separacion.Tiempo_licu) 
    
    informe.drawString(280,740,'PH:') 
    informe.drawString(380,740,'%s' % separacion.PH) 
    informe.drawString(470,740,'(>7,2)') 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,720,'Volumen:') 
    informe.drawString(120,720,'%s' % separacion.Volumen +" "+"ml") 
    informe.drawString(210,720,'(>1,5 ml)') 
    
    informe.drawString(280,720,'Viscosidad:') 
    informe.drawString(380,720,'%s' % separacion.Viscosidad) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,700,'Aspecto:') 
    informe.drawString(120,700,'%s' % separacion.get_Aspecto_display()) 
     
    informe.drawString(280,700,'C. gelatinosos:') 
    informe.drawString(380,700,'%s' % separacion.Cuerpos_gelatinosos) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,680,'Color:') 
    informe.drawString(120,680,'%s' % separacion.get_Color_display()) 
     
    informe.drawString(280,680,'C.en suspención:') 
    informe.drawString(380,680,'%s' % separacion.Cuerpos_en_suspen) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(280,660,'C.en mucosos:') 
    informe.drawString(380,660,'%s' % separacion.Cuerpos_mucosos) 

    #----------------------------------EXAME MICROSCÓPICO--------------------------------------------- 
    informe.line(30,650,580,650) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,630,'2.Examen microscópico') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,620,'Valor ref') 
    informe.drawString(470,620,'Valor ref') 
    informe.setFont('Helvetica-Bold', 9) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,600,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(193,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(180,600,'x10 /ml') 
    informe.drawString(160,600,'%s' % separacion.Concentracion) 
    

    informe.setFont('Helvetica', 5) 
    informe.drawString(248,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(235,600,'x10 /ml)') 
    informe.setFont('Helvetica', 8)
    informe.drawString(215,600,'(>15') 
     
    informe.drawString(280,600,'Células redondas:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,600,'x10 /ml') 
    informe.setFont('Helvetica', 8)
    informe.drawString(380,600,'%s' % separacion.Celulas_redondas) 
    
    informe.setFont('Helvetica', 5) 
    informe.drawString(500,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(487,600,'x10 /ml)') 
    informe.setFont('Helvetica', 8)
    informe.drawString(470,600,'(<1') 
    
    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,580,'Recuento total:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(158,585,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(145,580,'x10') 
    informe.setFont('Helvetica', 8)  
    informe.drawString(120,580,'%s' % separacion.Total) 
    
    informe.setFont('Helvetica', 5) 
    informe.drawString(243,585,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(230,580,'x10  )') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(210,580,'(>39') 
     
    informe.drawString(280,580,'Detritus:') 
    informe.drawString(380,580,'%s' % separacion.get_Detritus_display()) 
 
    #------------------------------------------------------------------------------------------------ 
    informe.drawString(280,560,'Células escamosas:') 
    informe.drawString(380,560,'%s' % separacion.Celulas_escamosas) 

    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,540,'Vitalidad:') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(120,540,'%s' % separacion.Vitalidad+" "+"%") 
    informe.drawString(210,540,'(>58%)') 

    informe.drawString(280,540,'Aglutinación:') 
    informe.drawString(380,540,'%s' % separacion.get_Aglutinacion_display()) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,520,'Pseudoaglutinación:') 
    informe.drawString(380,520,'%s' % separacion.Pseudoaglutinacion) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,500,'Cristales:') 
    informe.drawString(380,500,'%s' % separacion.Cristales) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,480,'Mucus:') 
    informe.drawString(380,480,'%s' % separacion.Mucus) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(280,460,'Bacterias:') 
    informe.drawString(380,460,'%s' % separacion.Bacterias) 

    #----------------------------------MOTILIDAD--------------------------------------------- 
    informe.line(30,450,580,450) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,430,'2.Motilidad') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,420,'Valor ref') 
    informe.setFont('Helvetica', 9) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,400,'Progresivos:') 
    informe.drawString(120,400,'%s' % separacion.Progresivos +" "+"%") 
    informe.drawString(210,400,'(>32%)') 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,380,'No progresivos:') 
    informe.drawString(120,380,'%s' % separacion.No_progresivos +" "+"%") 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,360,'Inmóviles:') 
    informe.drawString(120,360,'%s' % separacion.Inmoviles +" "+"%") 

    #-----------------------------MORFOLOGÍA------------------------------------ 
    informe.line(30,350,580,350) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,330,'4.Morfología') 
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,320,'Valor ref') 
    informe.drawString(470,320,'Valor ref') 
    informe.setFont('Helvetica', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,300,'Normales:') 
    informe.drawString(120,300,'%s' % separacion.Normales +" "+"%") 
    informe.drawString(210,300,'(>4%)') 
     
    informe.drawString(280,300,'Defecto cabeza:') 
    informe.drawString(380,300,'%s' % separacion.Defecto_cabeza) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,280,'Anormales:') 
    informe.drawString(120,280,'%s' % separacion.Anormales+" "+"%") 
     
    informe.drawString(280,280,'Defecto pieza 1/2:') 
    informe.drawString(380,280,'%s' % separacion.Defecto_pieza)  
     
    #------------------------------------------------------------------------------------------------- 

    informe.drawString(280,260,'Defecto cola:') 
    informe.drawString(380,260,'%s' % separacion.Defecto_cola) 


    #-----------------------------------------------TEM----------------------------------------------- 
    informe.line(30,250,580,250) 
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,235,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,230,'x10') 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,230,'TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) EN LA MUESTRA SEMINAL: %s' %  separacion.TEM) 

    #--------------------------------------------PROTOCOLO-------------------------------------------- 
    informe.line(30,220,580,220) 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,200,'SEPARACIÓN ESPERMÁTICA MEDIANTE PROTOCOLO DE: %s' %  str(separacion.get_Protocolo_display()))
    informe.line(30,190,580,190) 
    informe.showPage() 

    #--------------------------------------------SEGUNDA PÁGINA--------------------------------------------
    informe.drawImage('static/img/uv.jpeg',30,950,90,45) 
    informe.setLineWidth(.3) 
    informe.setFont('Helvetica-Bold', 12) 
    informe.drawString(180,950,'INFORME SEPARACIÓN ESPERMÁTICA') 
    informe.setFont('Helvetica', 8)

    #-------------------------------------EXAMEN MICROSCÓPICO POST-SEPARACIÓN-------------------------------
    informe.line(30,920,580,920) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,900,'5.Examen microscópico post-separación') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(260,880,'Valor ref') 
    
    informe.setFont('Helvetica', 8) 
    informe.drawString(30,860,'Volumen:') 
    
    informe.drawString(160,860,'%s' % separacion.Volumen_post+" "+"ml") 

    informe.setFont('Helvetica', 5) 
    informe.drawString(293,845,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(280,840,'x10 /ml)') 
    informe.drawString(260,840,'(>15') 
    
    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,840,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(198,845,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(185,840,'x10 /ml') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(160,840,'%s' % separacion.Concentracion_post) 

   

    #----------------------------------MOTILIDAD POST-SEPARACIÓN--------------------------------------------- 
    informe.line(30,830,580,830) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,810,'6.Motilidad') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(210,800,'Valor ref') 
    informe.setFont('Helvetica', 9) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,780,'Progresivos:') 
    informe.drawString(120,780,'%s' % separacion.Progresivos_post +" "+"%") 
    informe.drawString(210,780,'(>32%)') 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,760,'No progresivos:') 
    informe.drawString(120,760,'%s' % separacion.No_progresivos_post +" "+"%") 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,740,'Inmóviles:') 
    informe.drawString(120,740,'%s' % separacion.Inmoviles_post +" "+"%") 

    #-----------------------------------------------TEM----------------------------------------------- 
    informe.line(30,730,580,730)  
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,715,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,710,'x10') 
    informe.setFont('Helvetica-Bold', 9)
    informe.drawString(30,710,'TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) EN LA MUESTRA SEMINAL: %s' %  separacion.TEM_post) 

    #-----------------------------------------------DIAGNOSTICO----------------------------------------------- 
    informe.line(30,700,580,700) 
    informe.drawString(30,680,'Diágnostico:')
    informe.drawString(150,680,'Observación:')
    informe.setFont('Helvetica', 8)
    j= 660
    for i in separacion.diagnostico_se_set.all() :
        informe.drawString(30,j,'%s' % i.get_Diagnostico_display())
        informe.drawString(150,j,'%s' % i.Observacion)
        j = j - 20

    #-----------------------------------------------COMENTARIO----------------------------------------------- 
    informe.line(30,j,580,j)
    informe.setFont('Helvetica',8)
    j=j-20 
    informe.drawString(30,j,'Valeria Andrea Concha Stark')
    informe.drawString(30,j-20,'Tecnólogo Médico - Laboratorio de Andrología')


    informe.showPage() 
    informe.save() 
  
    return response


def informe_separacion_o(request, separacion_id): 
    separacion = get_object_or_404(Separacion_o,pk=separacion_id) 
    paciente =  (separacion.Info_examen.Paciente.Nombres.replace(' ', '_')) +"_"+separacion.Info_examen.Paciente.Primer_apellido 
    nombre = "so_" + paciente + ".pdf" 
    response = HttpResponse(content_type='application/pdf') 
    response['Content-Disposition'] = 'attachment; filename=%s' % nombre 
    informe = canvas.Canvas(response, pagesize= legal) 
    informe.drawImage('static/img/uv.jpeg',30,950,90,45) 
    informe.setLineWidth(.3) 
    informe.setFont('Helvetica-Bold', 12) 
    informe.drawString(150,950,'INFORME SEPARACIÓN ESPERMÁTICA EN ORINA') 
    informe.setFont('Helvetica', 8) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,920,'Fecha:') 
    informe.drawString(80,920,'%s' % separacion.Info_examen.Fecha.strftime('%d-%m-%Y')) 
    
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,900,'Nombre:') 
    nombre = separacion.Info_examen.Paciente.Nombres +" "+separacion.Info_examen.Paciente.Primer_apellido+" "+separacion.Info_examen.Paciente.Segundo_apellido 
    informe.drawString(80,900,'%s' % nombre) 
    
    if (separacion.Info_examen.Paciente.RUN):
        informe.drawString(320,900,'Rut:') 
        rut = str(separacion.Info_examen.Paciente.RUN) +"-"+separacion.Info_examen.Paciente.DV 
        informe.drawString(370,900,'%s' % rut) 
    else:
        informe.drawString(320,900,'DI:') 
        di = str(separacion.Info_examen.Paciente.DI) 
        informe.drawString(370,900,'%s' % di) 


    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica', 8) 
    informe.drawString(30,880,'Edad:') 
    informe.drawString(80,880,'%s' % separacion.Info_examen.Paciente.Edad) 
    
    if (separacion.Info_examen.Paciente.Conyuge):
        informe.drawString(320,880,'Cónyuge:') 
        conyuge = separacion.Info_examen.Paciente.Conyuge.Nombres +" "+separacion.Info_examen.Paciente.Conyuge.Primer_apellido+" "+separacion.Info_examen.Paciente.Conyuge.Segundo_apellido 
        informe.drawString(370,880,'%s' % conyuge) 

    #------------------------------------------------------------------------------------------------- 
    recientes = Antecedentes_recientes.objects.filter(Examen= separacion.Info_examen)
    if (recientes):
        informe.drawString(30,860,'Médico:') 
        informe.drawString(80,860,'%s' % recientes[0].Medico_tratante)
    
    if (separacion.Info_examen.Paciente.Telefono):
        informe.drawString(320,860,'Telefono:') 
        telefono = str(separacion.Info_examen.Paciente.Codigo) +"-"+str(separacion.Info_examen.Paciente.Telefono) 
        informe.drawString(370,860,'%s' % telefono) 

    #------------------------------------------------------------------------------------------------- 
    antecedentes_h = Antecedentes_habitos.objects.filter(Paciente= separacion.Info_examen.Paciente)
    if (antecedentes_h):
        habito = "Tabaco: "+antecedentes_h[0].get_Tabaco_display()+", Alcohol: "+ antecedentes_h[0].get_Alcohol_display()
        habito2 = " "
        habito3 = " "
        if (antecedentes_h[0].Marihuana):
            habito = str(habito) + ", Marihuana: "+ str(antecedentes_h[0].get_Marihuana_display())
        if (antecedentes_h[0].Pasta):
            habito2 = "Pasta: "+ str(antecedentes_h[0].get_Pasta_display())
        if (antecedentes_h[0].Cocaina):
            habito2 =str(habito2) + ", Cocaína: "+ str(antecedentes_h[0].get_Cocaina_display())
        if (antecedentes_h[0].Drogas):
            habito2 = str(habito2) +", Otras drogas:" + str(antecedentes_h[0].Drogas)
        if (antecedentes_h[0].Radiacion):
            habito3 = "Radiación: Sí ,"
        if (antecedentes_h[0].Quimicos):
            habito3 = str(habito3) + "Químicos: Sí."

        informe.drawString(30,840,'Hábitos:') 
        informe.drawString(80,840,'%s' % habito) 
        informe.drawString(80,830,'%s' % habito2)

        informe.drawString(30,820,'Exposición:') 
        informe.drawString(80,820,'%s' % habito3)
   
 
    informe.drawString(320,840,'Hora:') 

    informe.drawString(370,840,'%s' % separacion.Info_examen.Fecha.strftime('%X')) 
    #------------------------------------------------------------------------------------------------- 
    if (recientes):   
        informe.drawString(30,800,'Días de abstinencia:') 
        informe.drawString(110,800,'%s' % recientes[0].Abstinencia)
    
   
    antecedentes_t = Antecedentes_testiculares.objects.filter(Paciente= separacion.Info_examen.Paciente)
    if (antecedentes_t):
        testicular= " "
        if (antecedentes_t[0].Varicocele):
            testicular = "Varicocele ,"
        if (antecedentes_t[0].Hidrocele):
            testicular = str(testicular) +"Hidrocele ,"
        if (antecedentes_t[0].Criptorquidia):
            testicular = str(testicular) +"Criptorquidia ,"
        if (antecedentes_t[0].Tumores):
            testicular = str(testicular) +"Tumores ,"
        if (antecedentes_t[0].Golpes_testiculares):
            testicular = str(testicular) +"Golpes testiculares."
        informe.drawString(320,820,'Otros:') 
        informe.drawString(370,820,'%s' % testicular) 
   

    #-----------------------------EXAME MACROSCOPICO------------------------------------ 
    informe.line(30,790,580,790) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,770,'1.Examen macroscópico orina') 
    
    #------------------------------------------------------------------------------------------------- 
   
    informe.setFont('Helvetica', 8) 
    informe.drawString(30,740,'Volumen:') 
    informe.drawString(120,740,'%s' % separacion.Volumen +" "+"ml") 
    


    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,720,'Aspecto:') 
    informe.drawString(120,720,'%s' % separacion.get_Aspecto_display()) 
     
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,700,'Color:') 
    informe.drawString(120,700,'%s' % separacion.get_Color_display()) 
     
    

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,680,'PH') 
    informe.drawString(120,680,'%s' % separacion.PH) 

    informe.drawString(280,680,'Paciente cumple con la indicación de toma de bicarconato:') 
    informe.drawString(500,680,'%s' % separacion.Bicarbonato) 

    #----------------------------------EXAME MICROSCÓPICO--------------------------------------------- 
    informe.line(30,670,580,670) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,650,'2.Examen microscópico orina pre-separación') 
    informe.setFont('Helvetica-Bold', 8) 

    #-------------------------------------------------------------------------------------------------  
    informe.setFont('Helvetica-Bold', 9)
    informe.drawString(30,620,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(198,625,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(185,620,'x10 /ml') 
    informe.drawString(160,620,'%s' % separacion.Concentracion) 
    
    
   
    
    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,600,'Recuento total:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(158,605,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(145,600,'x10') 
    informe.drawString(120,600,'%s' % separacion.Total) 
    

    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,580,'Vitalidad:') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(120,580,'%s' % separacion.Vitalidad+" "+"%") 
    

    #---------------------------550-------MOTILIDAD--------------------------------------------- 
    informe.line(30,570,580,570)
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,550,'2.Motilidad') 
    informe.setFont('Helvetica-Bold', 8) 


    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,520,'Progresivos:') 
    informe.drawString(120,520,'%s' % separacion.Progresivos +" "+"%") 
    

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,500,'No progresivos:') 
    informe.drawString(120,500,'%s' % separacion.No_progresivos +" "+"%") 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,480,'Inmóviles:') 
    informe.drawString(120,480,'%s' % separacion.Inmoviles +" "+"%") 

    #-----------------------------MORFOLOGÍA------------------------------------ 
    informe.line(30,470,580,470) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,450,'4.Morfología') 
  
    #------------------------------------------------------------------------------------------------- 
   
    informe.setFont('Helvetica', 8) 
    informe.drawString(30,420,'Normales:') 
    informe.drawString(120,420,'%s' % separacion.Normales +" "+"%") 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,400,'Anormales:') 
    informe.drawString(120,400,'%s' % separacion.Anormales+" "+"%")  
     

    #-----------------------------------------------TEM----------------------------------------------- 
    informe.line(30,390,580,390)
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,375,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,370,'x10') 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,370,'TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) EN LA MUESTRA DE ORINA: %s' %  separacion.TEM) 

    #--------------------------------------------PROTOCOLO-------------------------------------------- 
    informe.line(30,360,580,360) 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,340,'Observación: %s' %  str(separacion.Observacion))
    informe.line(30,330,580,330) 
    informe.showPage() 

    #--------------------------------------------SEGUNDA PÁGINA--------------------------------------------
    informe.drawImage('static/img/uv.jpeg',30,950,90,45) 
    informe.setLineWidth(.3) 
    informe.setFont('Helvetica-Bold', 12) 
    informe.drawString(150,950,'INFORME SEPARACIÓN ESPERMÁTICA EN ORINA')  
    informe.setFont('Helvetica', 8)

    #-------------------------------------EXAMEN MICROSCÓPICO POST-SEPARACIÓN-------------------------------
    informe.line(30,920,580,920) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,900,'5.Examen microscópico post-separación') 
    informe.setFont('Helvetica', 8) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,870,'Volumen:') 
    informe.drawString(120,870,'%s' % separacion.Volumen_post +" "+"ml")

    informe.setFont('Helvetica-Bold', 9) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,845,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(198,850,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(185,845,'x10 /ml') 
    informe.drawString(160,845,'%s' % separacion.Concentracion_post) 
    

    #----------------------------------MOTILIDAD POST-SEPARACIÓN--------------------------------------------- 
    informe.line(30,830,580,830) 
    informe.setFont('Helvetica-Bold', 11) 
    
    informe.drawString(30,810,'6.Motilidad') 
    informe.setFont('Helvetica-Bold', 8) 

    #------------------------------------------------------------------------------------------------- 
    
    informe.setFont('Helvetica', 9) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,780,'Progresivos:') 
    informe.drawString(120,780,'%s' % separacion.Progresivos_post +" "+"%") 
   

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,760,'No progresivos:') 
    informe.drawString(120,760,'%s' % separacion.No_progresivos_post +" "+"%") 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,740,'Inmóviles:') 
    informe.drawString(120,740,'%s' % separacion.Inmoviles_post +" "+"%") 

    #-----------------------------------------------TEM----------------------------------------------- 
    informe.line(30,730,580,730) 
    informe.setFont('Helvetica', 5) 
    informe.drawString(418,715,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(405,710,'x10') 
    informe.setFont('Helvetica-Bold', 9)
    informe.drawString(30,710,'TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) EN LA MUESTRA DE ORINA: %s' %  separacion.TEM_post) 

    #-----------------------------------------------DIAGNOSTICO----------------------------------------------- 
    informe.line(30,700,580,700) 
    informe.drawString(30,680,'Diágnostico:')
    informe.drawString(150,680,'Observación:')
    informe.setFont('Helvetica', 8)
    j= 660
    for i in separacion.diagnostico_so_set.all() :
        informe.drawString(30,j,'%s' % i.get_Diagnostico_display())
        informe.drawString(150,j,'%s' % i.Observacion)
        j = j - 20

    #-----------------------------------------------COMENTARIO----------------------------------------------- 
    informe.line(30,j,580,j)
    informe.setFont('Helvetica',8)
    j=j-20 
    informe.drawString(30,j,'Valeria Andrea Concha Stark')
    informe.drawString(30,j-20,'Tecnólogo Médico - Laboratorio de Andrología')


    informe.showPage() 
    informe.save() 
    return response


def informe_inseminacion(request, inseminacion_id): 
    inseminacion = get_object_or_404(Inseminacion,pk=inseminacion_id) 
    paciente =  (inseminacion.Info_examen.Paciente.Nombres.replace(' ', '_')) +"_"+inseminacion.Info_examen.Paciente.Primer_apellido 
    nombre = "iiu_" + paciente + ".pdf" 
    response = HttpResponse(content_type='application/pdf') 
    response['Content-Disposition'] = 'attachment; filename=%s' % nombre 
    informe = canvas.Canvas(response, pagesize= legal) 
    informe.drawImage('static/img/uv.jpeg',30,950,90,45) 
    informe.setLineWidth(.3) 
    informe.setFont('Helvetica-Bold', 12) 
    informe.drawString(220,950,'INSEMINACIÓN INTRAUTERINA') 
    informe.setFont('Helvetica', 8) 
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,900,'Fecha:') 
    informe.drawString(100,900,'%s' % inseminacion.Info_examen.Fecha.strftime('%d-%m-%Y')) 
    
    informe.setFont('Helvetica-Bold', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,880,'Nombre:') 
    nombre = inseminacion.Info_examen.Paciente.Nombres +" "+inseminacion.Info_examen.Paciente.Primer_apellido+" "+inseminacion.Info_examen.Paciente.Segundo_apellido 
    informe.drawString(100,880,'%s' % nombre) 
    
    if (inseminacion.Info_examen.Paciente.RUN):
        informe.drawString(300,880,'Rut:') 
        rut = str(inseminacion.Info_examen.Paciente.RUN) +"-"+inseminacion.Info_examen.Paciente.DV 
        informe.drawString(370,880,'%s' % rut) 
    else:
        informe.drawString(300,880,'DI:') 
        di = str(inseminacion.Info_examen.Paciente.DI) 
        informe.drawString(370,880,'%s' % di)
    #------------------------------------------------------------------------------------------------- 
    informe.setFont('Helvetica', 8)

    if (inseminacion.Info_examen.Paciente.Conyuge): 
        informe.drawString(30,860,'Cónyuge:') 
        conyuge = inseminacion.Info_examen.Paciente.Conyuge.Nombres +" "+inseminacion.Info_examen.Paciente.Conyuge.Primer_apellido+" "+inseminacion.Info_examen.Paciente.Conyuge.Segundo_apellido 
        informe.drawString(100,860,'%s' % conyuge) 
        if (inseminacion.Info_examen.Paciente.Conyuge.RUN):
            informe.drawString(300,860,'Rut:') 
            rut = str(inseminacion.Info_examen.Paciente.Conyuge.RUN) +"-"+inseminacion.Info_examen.Paciente.Conyuge.DV 
            informe.drawString(370,860,'%s' % rut) 
        else:
            informe.drawString(300,860,'DI:') 
            di = str(inseminacion.Info_examen.Paciente.Conyuge.DI) 
            informe.drawString(370,860,'%s' % di)

    #------------------------------------------------------------------------------------------------- 
    
    recientes = Antecedentes_recientes.objects.filter(Examen= inseminacion.Info_examen)
    if (recientes):
        informe.drawString(30,840,'Médico:') 
        informe.drawString(100,840,'%s' % recientes[0].Medico_tratante)
   
    #informe.drawString(300,840,'Telefono:') 
    #telefono = str(inseminacion.Info_examen.Paciente.Codigo) +"-"+str(inseminacion.Info_examen.Paciente.Telefono) 
    #informe.drawString(370,840,'%s' % telefono) 

    #------------------------------------------------------------------------------------------------- 
    #informe.drawString(30,820,'Hábitos:') 
    #informe.drawString(100,820,'Qué habitos van aquí?') 
    
    #informe.drawString(300,820,'Hora:') 
    #informe.drawString(370,820,'cortar la hora de a fecha datestamp' ) 

    

    #-----------------------------DATOS SEMEN NATIVO------------------------------------ 
    informe.line(30,830,580,830) 
    
    
    informe.drawString(30,810,'La unidad de Medicina Reproductiva de la Universidad e Valparaiso, certifica que a la paciente, se le realizó') 
    informe.drawString(30,800,'un procedimiento de inseminación intrauterina. Los valores de la muestra espermática utilizada fueron:') 
    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,780,'DATOS SEMEN NATIVO:')
    informe.setFont('Helvetica', 8)  
    #------------------------------------------------------------------------------------------------- 
    if (recientes):    
        informe.drawString(30,760,'Días de abstinencia:') 
        informe.drawString(200,760,'%s' % recientes[0].Abstinencia) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,740,'Volumen:') 
    informe.drawString(200,740,'%s' % inseminacion.Volumen +" "+"ml")   

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,720,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(238,725,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(225,720,'x10 /ml')
    informe.drawString(200,720,'%s' % inseminacion.Concentracion) 

    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,700,'Recuento total de espermatozoides:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(238,705,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(225,700,'x10') 
    informe.drawString(200,700,'%s' % inseminacion.Total) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,680,'Motilidad espermatozoides progresivos:') 
    informe.drawString(200,680,'%s' % inseminacion.Progresivos +" "+"%") 

    #-----------------------------------------------TEM----------------------------------------------- 
    informe.setFont('Helvetica-Bold', 8)
    informe.drawString(30,660,'Total Espermatozoides Mótiles Progresivos:'  ) 
    informe.setFont('Helvetica', 5) 
    informe.drawString(238,665,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(225,660,'x10')
    informe.drawString(200,660,'%s' % inseminacion.TEM) 

     #-----------------------------DATOS SEMEN POST-SEPARACIÓN ESPERMÁTICA------------------------------------ 

    informe.setFont('Helvetica-Bold', 9) 
    informe.drawString(30,620,'DATOS SEMEN POST-SEPARACIÓN ESPERMÁTICA:') 
    informe.setFont('Helvetica', 8) 
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,600,'Volumen:') 
    informe.drawString(200,600,'%s' % inseminacion.Volumen_post +" "+"ml")   

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,580,'Concentración espermática:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(238,585,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(225,580,'x10 /ml')
    informe.drawString(200,580,'%s' % inseminacion.Concentracion_post)  
    
    #------------------------------------------------------------------------------------------------- 
    informe.drawString(30,560,'Recuento total de espermatozoides:') 
    informe.setFont('Helvetica', 5) 
    informe.drawString(238,565,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(225,560,'x10')
    informe.drawString(200,560,'%s' % inseminacion.Total_post) 

    #-------------------------------------------------------------------------------------------------  
    informe.drawString(30,540,'Motilidad espermatozoides progresivos:') 
    informe.drawString(200,540,'%s' % inseminacion.Progresivos_post +" "+"%") 

    #-----------------------------------------------TEM----------------------------------------------- 
    informe.setFont('Helvetica-Bold', 8)
    informe.drawString(30,520,'Total Espermatozoides Mótiles Progresivos:'  ) 
    
    informe.setFont('Helvetica', 5) 
    informe.drawString(238,525,'6') 
    informe.setFont('Helvetica', 8) 
    informe.drawString(225,520,'x10') 
    informe.drawString(200,520,'%s' % inseminacion.TEM_post) 
    #-----------------------------------------------DATOS PROCEDIMIENTO----------------------------------------------- 
    informe.line(30,510,580,510)
    recientes = Antecedentes_recientes.objects.filter(Examen= inseminacion.Info_examen)
    if (recientes):
        informe.drawString(30,490,'La inseminación intrauterina fue realizada por:')  
        informe.drawString(200,490,'%s' % recientes[0].Medico_tratante)


    informe.drawString(30,470,'Hora inseminación:') 
    informe.drawString(200,470,'%s' % inseminacion.Info_examen.Fecha.strftime('%X')) 

    informe.drawString(30,450,'Protocolo preparación muestra espermática:') 
    informe.drawString(200,450,'%s' % str(inseminacion.get_Protocolo_display())) 
    #-----------------------------------------------DIAGNOSTICO----------------------------------------------- 

    informe.line(30,440,580,440)
    informe.setFont('Helvetica-Bold', 9)
    informe.drawString(30,420,'Observaciones:')
    informe.setFont('Helvetica', 8) 
    informe.drawString(200,420,'%s' % inseminacion.Observacion ) 
    
    informe.line(30,400,580,400) 
    
    informe.setFont('Helvetica', 8)
    
    j= 400
     
   
    informe.setFont('Helvetica',8)
    j=j-20 
    informe.drawString(30,j,'Valeria Andrea Concha Stark')
    informe.drawString(30,j-20,'Tecnólogo Médico - Laboratorio de Andrología')


    informe.showPage() 
    informe.save() 
    return response


