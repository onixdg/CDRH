#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes
#url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),
url(r'^informe_espermiograma/(?P<espermio_id>[-\w]+)/$', views.informe_espermiograma, name='informe_espermiograma'),
url(r'^informe_separacion/(?P<separacion_id>[-\w]+)/$', views.informe_separacion, name='informe_separacion'),
url(r'^informe_separacion_o/(?P<separacion_id>[-\w]+)/$', views.informe_separacion_o, name='informe_separacion_o'),
url(r'^informe_inseminacion/(?P<inseminacion_id>[-\w]+)/$', views.informe_inseminacion, name='informe_inseminacion'),

]