# -*- coding: UTF-8 -*-
from django import forms
from .models import *
from paciente.models import Paciente
from hora.models import Examen
from django.core.exceptions import ValidationError


# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class habitos_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_habitos
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'


	def clean_Paciente(self):
		diccionario_limpio = self.cleaned_data
		Paciente = diccionario_limpio.get("Paciente")
		if (Paciente == None or (len(str(Paciente))==0) ) :
				raise forms.ValidationError("El paciente es requerido")
		return Paciente

	def clean_Tipo(self):
		diccionario_limpio = self.cleaned_data
		Tipo = diccionario_limpio.get("Tipo")
		if (Tipo == None or (len(Tipo)==0)  ) :
				raise forms.ValidationError("El examen es requerido")
		return Tipo

class medicos_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_medicos
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class testiculares_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_testiculares
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class recientes_form (forms.ModelForm):

	class Meta:
		model = Antecedentes_recientes
		
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'