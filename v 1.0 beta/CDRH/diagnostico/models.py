# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from paciente.models import Paciente as pac
from andrologia.models import Espermiograma as esp
from andrologia.models import Separacion as se
from andrologia.models import Separacion_o as so
from andrologia.models import Inseminacion as iiu
from django.utils import timezone

# Create your models here.
class Diagnostico_esp(models.Model):
	Espermiograma = models.ForeignKey(esp)
	DIAGNOSTICO = (
		('olizo','Oligozoospermia'),
		('astzo','Astenozoospermia'),
		('ter','Teratozoospermia'),
		('olias','Oligoastenozoospermia'),
		('olite','Oligoteratozoospermia'),
		('oliat','Oligoastenoteratozoospermia'),
		('astte','Astenoteratozoospermia'),
		('nec','Necrozoospermia'), 
		('azo','Azoospermia'),
		('hip','Hipospermia'),
		('asp','Aspermia'),
		('leu','Leucocitospermia'),
		('hem','Hematospermia'),
		)
	Diagnostico = models.CharField("Diagnóstico",max_length = 5,blank = True,choices = DIAGNOSTICO)
	Observacion = models.TextField()

class Diagnostico_se(models.Model):
	Separacion = models.ForeignKey(se)
	DIAGNOSTICO = (
		('olizo','Oligozoospermia'),
		('astzo','Astenozoospermia'),
		('ter','Teratozoospermia'),
		('olias','Oligoastenozoospermia'),
		('olite','Oligoteratozoospermia'),
		('oliat','Oligoastenoteratozoospermia'),
		('astte','Astenoteratozoospermia'),
		('nec','Necrozoospermia'), 
		('azo','Azoospermia'),
		('hip','Hipospermia'),
		('asp','Aspermia'),
		('leu','Leucocitospermia'),
		('hem','Hematospermia'),
		)
	Diagnostico = models.CharField("Diagnóstico",max_length = 5,blank = True,choices = DIAGNOSTICO)
	Observacion = models.TextField()

class Diagnostico_so(models.Model):
	Separacion_o = models.ForeignKey(so)
	DIAGNOSTICO = (
		('+','Eyaculación retrógrada positiva'),
		('-','Eyaculación retrógrada negativa'),
		)
	Diagnostico = models.CharField("Diagnóstico",max_length = 5,blank = True,choices = DIAGNOSTICO)
	Observacion = models.TextField()

class Diagnostico_iiu(models.Model):
	Inseminacion = models.ForeignKey(iiu)
	DIAGNOSTICO = (
		('olizo','Oligozoospermia'),
		('astzo','Astenozoospermia'),
		('ter','Teratozoospermia'),
		('olias','Oligoastenozoospermia'),
		('olite','Oligoteratozoospermia'),
		('oliat','Oligoastenoteratozoospermia'),
		('astte','Astenoteratozoospermia'),
		('nec','Necrozoospermia'), 
		('azo','Azoospermia'),
		('hip','Hipospermia'),
		('asp','Aspermia'),
		('leu','Leucocitospermia'),
		('hem','Hematospermia'),
		)
	Diagnostico = models.CharField("Diagnóstico",max_length = 5,blank = True,choices = DIAGNOSTICO)
	Observacion = models.TextField()