# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from paciente.models import Paciente as pac
from hora.models import Examen as exm
from django.utils import timezone



#---------------------------------------------CLASE ESPERMIOGRAMA---------------------------------------- 
#Clase enfocada a los detalles del informe del examen de tipo espermiograma
class Espermiograma(models.Model):
	Info_examen = models.ForeignKey(exm)
	#Paciente = models.ForeignKey(Paciente)
	#Hora = models.ForeignKey(Hora)

	#rango numérico del tiempo de licufaccion 
	#TIEMPO_LICU = (15,20,25,30,35,40,45,50,55,60)
	#opciones de aspecto de la muestra
	ASPECTO = (
		('ho' , 'homogéneo'),
		('he' , 'heterogéneo'),
		('hof' , 'homogéneo filante'),
		('hef' , 'heterogéneo filante'),
		('t' , 'traslúcido'),
		('e' , 'espumoso'),
		('o' , 'otros'),
		)
	#opciones de color de la muestra
	COLOR = (
		('n' , 'normal'),
		('a' , 'amarillento'),
		('b' , 'blanquecino'),
		('r' , 'rojizo'),
		('o' , 'otros'),
		)
	#Viscosidad presente en la muestra
	VISCOSIDAD = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)	
	#Cuerpos gelatinoso presentes en la muestra
	C_GELATINOSOS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Cuerpos en suspención presentes en la muestra
	C_EN_SUSPEN = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Cuerpos en suspención presentes en la muestra
	C_MUCOSOS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Detritus
	DETRITUS = (
		('n' , 'negativo'),
		('l' , 'leve'),
		('m' , 'moderado'),
		('a' , 'abundante'),
		)
	#Células escamosas
	C_ESCAMOSAS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	AGLUTINACION = (
		('n' , 'negativo'),
		('coc' , 'cola-cola'),
		('cac' , 'cabeza-cabeza'),
		('coca' , 'cola-cabeza'),
		)
	PSEUDOAGLUTINACION = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	CRISTALES = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	MUCUS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	BACTERIAS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Guarda la hora en que se finaliza informe
	#Hora_termino = models.DateTimeField(auto_now_add=True)
	#-------------------------------EXAMEN MACROSCÓPICO---------------------------------------
	Tiempo_licu = models.IntegerField("Tiempo Licuefacción",blank = True,null = True)
	Volumen = models.DecimalField(blank = True,null = True,max_digits=3, decimal_places=1) #recordar poner ml en el template
	Aspecto = models.CharField(max_length = 3,blank = True,null= True,choices = ASPECTO)
	Color = models.CharField(max_length = 1,blank = True,null = True,choices = COLOR)
	PH = models.FloatField(blank = True,null= True)
	Viscosidad = models.CharField(max_length = 1,blank = True,null = True,choices = VISCOSIDAD)
	Cuerpos_gelatinosos = models.CharField(max_length = 1,blank = True,null = True,choices = C_GELATINOSOS)
	Cuerpos_en_suspen = models.CharField("Cuerpos en suspensión",max_length = 1,blank = True,null = True,choices = C_EN_SUSPEN)
	Cuerpos_mucosos = models.CharField(max_length = 1,blank = True,null = True,choices = C_MUCOSOS)
	#-------------------------------EXAMEN MICROSCÓPICO---------------------------------------
	#concentración espermatica
	Concentracion = models.DecimalField("CONCENTRACIÓN ESPERMÁTICA",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Vitalidad = models.IntegerField("VITALIDAD",blank = True,null= True) #recordar poner el % en el template. (este valor se expresa en porcentaje)
	Celulas_redondas = models.IntegerField("Células redondas",blank = True,null = True) #puede ser negativo o un valor, Células
	Detritus = models.CharField(max_length = 1,blank = True,null = True,choices = DETRITUS)
	Celulas_escamosas = models.CharField("Células escamosas",max_length = 1,blank = True,null = True,choices = C_ESCAMOSAS)#Células escamosas
	Aglutinacion = models.CharField("Aglutinación",max_length = 4,blank = True,null = True,choices = AGLUTINACION)
	Pseudoaglutinacion = models.CharField("Pseudoaglutinación",max_length = 1,blank = True,null = True,choices = PSEUDOAGLUTINACION)
	Cristales = models.CharField(max_length = 1,blank = True,null = True,choices = CRISTALES)
	Mucus = models.CharField(max_length = 1,blank = True,null = True,choices=MUCUS)
	Bacterias = models.CharField(max_length = 1,blank = True,null = True,choices=BACTERIAS)
	#--------------------------------------MOTILIDAD-------------------------------------------
	Progresivos = models.IntegerField(blank = True,null = True) #recordar que es un %
	No_progresivos = models.IntegerField(blank=True,null = True) #recordar que es un %
	Inmoviles = models.IntegerField("Inmóviles",blank = True,null = True) # Inmóviles, recordar que es un %
	#--------------------------------------MORFOLOGÍA-------------------------------------------
	Normales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Anormales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Defecto_cabeza = models.IntegerField(blank = True,null = True)
	Defecto_pieza = models.IntegerField(blank = True,null = True)
	Defecto_cola = models.IntegerField(blank = True,null = True)
	#Total Concentracion x Volumen
	Total = models.DecimalField("RECUENTO TOTAL",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	TEM = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) en la muestra seminal",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#Metodos de la clase Espermiograma
	def recuento_total(self):
		recuento = self.Concentracion * self.Volumen
		return recuento #Recordar que el recuento va con X10⁶ en el template
	#def valor_cr(self):
		#if (self.Celulas_redondas == Null):
			# Celulas_redondas = -1


#	"""docstring for ClassName"""
#	def __init__(self, arg):
#		super(ClassName, self).__init__()
#		self.arg = arg


#---------------------------------------------CLASE SEPARACION---------------------------------------- 
#Clase enfocada a los detalles del informe del examen de tipo separación espermatica
class Separacion(models.Model):
	Info_examen = models.ForeignKey(exm)
	#Paciente = models.ForeignKey(Paciente)
	#Hora = models.ForeignKey(Hora)
	#rango numérico del tiempo de licufaccion 
	#TIEMPO_LICU = (15,20,25,30,35,40,45,50,55,60)
	#opciones de aspecto de la muestra
	ASPECTO = (
		('ho' , 'homogéneo'),
		('he' , 'heterogéneo'),
		('hof' , 'homogéneo filante'),
		('hef' , 'heterogéneo filante'),
		('t' , 'traslúcido'),
		('e' , 'espumoso'),
		('o' , 'otros'),
		)
	#opciones de color de la muestra
	COLOR = (
		('n' , 'normal'),
		('a' , 'amarillento'),
		('b' , 'blanquecino'),
		('r' , 'rojizo'),
		('o' , 'otros'),
		)
	#Viscosidad presente en la muestra
	VISCOSIDAD = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)	
	#Cuerpos gelatinoso presentes en la muestra
	C_GELATINOSOS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Cuerpos en suspención presentes en la muestra
	C_EN_SUSPEN = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Cuerpos en suspención presentes en la muestra
	C_MUCOSOS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Detritus
	DETRITUS = (
		('n' , 'negativo'),
		('l' , 'leve'),
		('m' , 'moderado'),
		('a' , 'abundante'),
		)
	#Células escamosas
	C_ESCAMOSAS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	AGLUTINACION = (
		('n' , 'negativo'),
		('coc' , 'cola-cola'),
		('cac' , 'cabeza-cabeza'),
		('coca' , 'cola-cabeza'),
		)
	PSEUDOAGLUTINACION = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	CRISTALES = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	MUCUS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	BACTERIAS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	PROTOCOLO =  (
		('gd45-90' , 'gradiente de densidad 45% - 90%'),
		('gd90' , 'gradiente de densidad 90%'),
		('su' , 'swim-up'),
		('ls' , 'lavado simple'),
		('o' , 'otros'),
		)
	
	#Guarda la hora en que se finaliza informe
	#Hora_termino = models.DateTimeField(auto_now_add=True)
	#-------------------------------EXAMEN MACROSCÓPICO---------------------------------------
	Tiempo_licu = models.IntegerField("Tiempo Licuefacción",blank = True,null = True)
	Volumen = models.DecimalField(blank = True,null = True,max_digits=3, decimal_places=1) #recordar poner ml en el template
	Aspecto = models.CharField(max_length = 3,blank = True,null= True,choices = ASPECTO)
	Color = models.CharField(max_length = 1,blank = True,null = True,choices = COLOR)
	PH = models.FloatField(blank = True,null= True)
	Viscosidad = models.CharField(max_length = 1,blank = True,null = True,choices = VISCOSIDAD)
	Cuerpos_gelatinosos = models.CharField(max_length = 1,blank = True,null = True,choices = C_GELATINOSOS)
	Cuerpos_en_suspen = models.CharField("Cuerpos en suspensión",max_length = 1,blank = True,null = True,choices = C_EN_SUSPEN)
	Cuerpos_mucosos = models.CharField(max_length = 1,blank = True,null = True,choices = C_MUCOSOS)
	#-------------------------------EXAMEN MICROSCÓPICO---------------------------------------
	#concentración espermatica
	Concentracion = models.DecimalField("CONCENTRACIÓN ESPERMÁTICA",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Vitalidad = models.IntegerField("VITALIDAD",blank = True,null= True) #recordar poner el % en el template. (este valor se expresa en porcentaje)
	Celulas_redondas = models.IntegerField("Células redondas",blank = True,null = True) #puede ser negativo o un valor, Células
	Detritus = models.CharField(max_length = 1,blank = True,null = True,choices = DETRITUS)
	Celulas_escamosas = models.CharField("Células escamosas",max_length = 1,blank = True,null = True,choices = C_ESCAMOSAS)#Células escamosas
	Aglutinacion = models.CharField("Aglutinación",max_length = 4,blank = True,null = True,choices = AGLUTINACION)
	Pseudoaglutinacion = models.CharField("Pseudoaglutinación",max_length = 1,blank = True,null = True,choices = PSEUDOAGLUTINACION)
	Cristales = models.CharField(max_length = 1,blank = True,null = True,choices = CRISTALES)
	Mucus = models.CharField(max_length = 1,blank = True,null = True,choices=MUCUS)
	Bacterias = models.CharField(max_length = 1,blank = True,null = True,choices=BACTERIAS)
	#--------------------------------------MOTILIDAD-------------------------------------------
	Progresivos = models.IntegerField(blank = True,null = True) #recordar que es un %
	No_progresivos = models.IntegerField(blank=True,null = True) #recordar que es un %
	Inmoviles = models.IntegerField("Inmóviles",blank = True,null = True) # Inmóviles, recordar que es un %
	#--------------------------------------MORFOLOGÍA-------------------------------------------
	Normales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Anormales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Defecto_cabeza = models.IntegerField(blank = True,null = True)
	Defecto_pieza = models.IntegerField(blank = True,null = True)
	Defecto_cola = models.IntegerField(blank = True,null = True)
	#Total Concentracion x Volumen
	Total = models.DecimalField("RECUENTO TOTAL",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	TEM = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) en la muestra seminal",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Protocolo = models.CharField(max_length = 7,blank = True,null = True,choices = PROTOCOLO)
	#--------------------------------------EXAMEN MICROSCÓPICO POST-SEPARACIÓN-------------------------------------------
	Concentracion_post = models.DecimalField("CONCENTRACIÓN ESPERMÁTICA",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#--------------------------------------MOTILIDAD POST-SEPARACIÓN-------------------------------------------
	Progresivos_post = models.IntegerField(blank = True,null = True) #recordar que es un %
	No_progresivos_post = models.IntegerField(blank=True,null = True) #recordar que es un %
	Inmoviles_post = models.IntegerField("Inmóviles",blank = True,null = True) # Inmóviles, recordar que es un %
	TEM_post = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) en la muestra seminal",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#Metodos de la clase Separacion
	def recuento_total(self):
		recuento = self.Concentracion * self.Volumen
		return recuento #Recordar que el recuento va con X10⁶ en el template
	#def valor_cr(self):
		#if (self.Celulas_redondas == Null):
			# Celulas_redondas = -1
 

#---------------------------------------------CLASE SEPARACION_O---------------------------------------- 
#Clase enfocada a los detalles del informe del examen de tipo separación espermatica en orina
class Separacion_o(models.Model):
	Info_examen = models.ForeignKey(exm)
	#Paciente = models.ForeignKey(Paciente)
	#Hora = models.ForeignKey(Hora)
	#rango numérico del tiempo de licufaccion 
	#TIEMPO_LICU = (15,20,25,30,35,40,45,50,55,60)
	#opciones de aspecto de la muestra
	ASPECTO = (
		('ho' , 'homogéneo'),
		('he' , 'heterogéneo'),
		('hof' , 'homogéneo filante'),
		('hef' , 'heterogéneo filante'),
		('t' , 'traslúcido'),
		('e' , 'espumoso'),
		('o' , 'otros'),
		)
	#opciones de color de la muestra
	COLOR = (
		('n' , 'normal'),
		('a' , 'amarillento'),
		('b' , 'blanquecino'),
		('r' , 'rojizo'),
		('o' , 'otros'),
		)
	BICARBONATO = (
		('si' , 'si'),
		('no' , 'no'),
		)
	
		
	#Guarda la hora en que se finaliza informe
	#Hora_termino = models.DateTimeField(auto_now_add=True)
	#-------------------------------EXAMEN MACROSCÓPICO---------------------------------------
	
	Volumen = models.DecimalField(blank = True,null = True,max_digits=3, decimal_places=1) #recordar poner ml en el template
	Aspecto = models.CharField(max_length = 3,blank = True,null= True,choices = ASPECTO)
	Color = models.CharField(max_length = 1,blank = True,null = True,choices = COLOR)
	PH = models.FloatField(blank = True,null= True)
	Bicarbonato = models.CharField(max_length = 2,blank = True,null = True,choices = BICARBONATO)
	#-------------------------------EXAMEN MICROSCÓPICO---------------------------------------
	#concentración espermatica
	Concentracion = models.DecimalField("CONCENTRACIÓN ESPERMÁTICA",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Vitalidad = models.IntegerField("VITALIDAD",blank = True,null= True) #recordar poner el % en el template. (este valor se expresa en porcentaje)
	
	#--------------------------------------MOTILIDAD-------------------------------------------
	Progresivos = models.IntegerField(blank = True,null = True) #recordar que es un %
	No_progresivos = models.IntegerField(blank=True,null = True) #recordar que es un %
	Inmoviles = models.IntegerField("Inmóviles",blank = True,null = True) # Inmóviles, recordar que es un %
	#--------------------------------------MORFOLOGÍA-------------------------------------------
	Normales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Anormales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Defecto_cabeza = models.IntegerField(blank = True,null = True)
	Defecto_pieza = models.IntegerField(blank = True,null = True)
	Defecto_cola = models.IntegerField(blank = True,null = True)
	#Total Concentracion x Volumen
	Total = models.DecimalField("RECUENTO TOTAL",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	TEM = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) en la muestra de orina",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Observacion = models.TextField(blank = True)
	#--------------------------------------EXAMEN MICROSCÓPICO ORINA POST-SEPARACIÓN-------------------------------------------
	Concentracion_post = models.DecimalField("CONCENTRACIÓN ESPERMÁTICA",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#--------------------------------------MOTILIDAD POST-SEPARACIÓN-------------------------------------------
	Progresivos_post = models.IntegerField(blank = True,null = True) #recordar que es un %
	No_progresivos_post = models.IntegerField(blank=True,null = True) #recordar que es un %
	Inmoviles_post = models.IntegerField("Inmóviles",blank = True,null = True) # Inmóviles, recordar que es un %
	TEM_post = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) en la muestra de orina",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#Metodos de la clase Separacion
	def recuento_total(self):
		recuento = self.Concentracion * self.Volumen
		return recuento #Recordar que el recuento va con X10⁶ en el template
	#def valor_cr(self):
		#if (self.Celulas_redondas == Null):
			# Celulas_redondas = -1


#---------------------------------------------CLASE INSEMINACION INTRAUTERINA---------------------------------------- 
#Clase enfocada a los detalles del informe del examen de tipo separación espermatica en orina
class Inseminacion(models.Model):
	Info_examen = models.ForeignKey(exm)
	PROTOCOLO =  (
		('gd45-90' , 'gradiente de densidad 45% - 90%'),
		('gd90' , 'gradiente de densidad 90%'),
		('su' , 'swim-up'),
		('ls' , 'lavado simple'),
		('o' , 'otros'),
		)
	#-------------------------------DATOS SEMEN NATIVO--------------------------------------------------------
	
	Volumen = models.DecimalField(blank = True,null = True,max_digits=3, decimal_places=1) #recordar poner ml en el template
	Total = models.DecimalField("RECUENTO TOTAL",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Progresivos = models.IntegerField(blank = True,null = True) #recordar que es un %
	TEM = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM) en la muestra de orina",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#--------------------------------------DATOS SEMEN POST-SEPARACIÓN ESPEMÁTICA---------------------------------------------------------
	Volumen_post = models.DecimalField(blank = True,null = True,max_digits=3, decimal_places=1) #recordar poner ml en el template
	Total_post = models.DecimalField("RECUENTO TOTAL",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	Progresivos_post = models.IntegerField(blank = True,null = True) #recordar que es un %
	TEM_post = models.DecimalField("TOTAL ESPERMATOZOIDES PROGRESIVOS (TEM)",blank = True,null = True,max_digits=3, decimal_places=1) #[valor]X10⁶
	#Metodos de la clase Separacion
	
	Protocolo = models.CharField(max_length = 7,blank = True,null = True,choices = PROTOCOLO)
	Observacion = models.TextField(blank = True)
	
	
	def recuento_total(self):
		recuento = self.Concentracion * self.Volumen
		return recuento #Recordar que el recuento va con X10⁶ en el template
	#def valor_cr(self):
		#if (self.Celulas_redondas == Null):
			# Celulas_redondas = -1


