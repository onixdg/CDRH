# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import paciente_form
#función listar_espermiogramas que se renderiza en template listar_espermio.html, lista todos los expermiogramas existentes
def listar_paciente(request):
	paciente = Paciente.objects.all()
	return render (request, "paciente/listar_paciente.html", {'paciente': paciente})

# Create your views here.
def crear_paciente(request):
 	
	 	if request.method =='POST':
	 		form = paciente_form(request.POST)
	 		
	 		if form.is_valid():
	 			conyuge = form.cleaned_data.get('Conyuge')
	 			form.save()
	 			if (conyuge):
	 				Conyuge = Paciente.objects.get(id = conyuge.id)
	 				Pareja =  Paciente.objects.last()
	 				
	 				conyuge.Conyuge = Pareja
	 				conyuge.save()
	 			
	 			return redirect('listar_paciente')
	 	else:
	 		
	 		form= paciente_form()
	 	return render (request, 'paciente/crear_paciente.html',{'form':form})
