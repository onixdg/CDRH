# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-24 17:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hora', '0005_auto_20160624_1753'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examen',
            name='Est_hora',
            field=models.CharField(choices=[('a', 'atendido'), ('e', 'en espera'), ('n', 'no se presenta')], default='e', max_length=3),
        ),
    ]
