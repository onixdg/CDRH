# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from hora.models import *
from antecedentes.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import *


def crear_antedentes_h(request, paciente_id):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		paciente=Paciente.objects.get(pk= paciente_id)
	 	if request.method =='POST':
	 		form = separacion_form(request.POST)
	 		if form.is_valid():
	 			form.save()
	 			return redirect('listar_separacion')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= separacion_form()
	 	return render (request, 'andrologia/crear_separacion.html',{'form':form, 'paciente':paciente_id})