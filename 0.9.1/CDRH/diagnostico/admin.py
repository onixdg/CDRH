from django.contrib import admin

# Register your models here.
from .models import *
# Register your models here
admin.site.register(Diagnostico_esp)
admin.site.register(Diagnostico_se)
admin.site.register(Diagnostico_so)
admin.site.register(Diagnostico_iiu)