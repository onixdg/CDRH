#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from .models import *
from django.core.exceptions import ValidationError

# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class diag_esp_form (forms.ModelForm):
	class Meta:
		model = Diagnostico_esp
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class diag_se_form (forms.ModelForm):
	class Meta:
		model = Diagnostico_se
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class diag_so_form (forms.ModelForm):
	class Meta:
		model = Diagnostico_so
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'

class diag_iiu_form (forms.ModelForm):
	class Meta:
		model = Diagnostico_iiu
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'
	