# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-22 21:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hora', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='examen',
            name='Tipo',
            field=models.CharField(blank=True, choices=[('es', 'Espermiograma'), ('eso', 'Espermiograma en orina'), ('se', 'Separaci\xf3n espermatica'), ('iiu', 'Inseminaci\xf3n intra uterina')], max_length=3),
        ),
    ]
