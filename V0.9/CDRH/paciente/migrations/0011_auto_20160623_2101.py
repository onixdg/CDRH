# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-23 21:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('paciente', '0010_auto_20160623_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paciente',
            name='Ciudad',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Codigo',
            field=models.IntegerField(blank=True, null=True, verbose_name='C\xf3digo'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Conyuge',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='paciente.Paciente'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='DV',
            field=models.CharField(blank=True, max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Direccion',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Edad',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Email',
            field=models.EmailField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Fecha_de_nacimiento',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Nombres',
            field=models.CharField(blank=True, error_messages={'required': 'Los nombres son requeridos.'}, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Origen',
            field=models.CharField(blank=True, choices=[('h', 'hospital Carlos Van Buren'), ('c', 'Centro De Reproducci\xf3n'), ('p', 'Particular')], max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Primer_apellido',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Apellido paterno'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='RUN',
            field=models.IntegerField(blank=True, null=True, verbose_name='RUT'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Segundo_apellido',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Apellido materno'),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Sexo',
            field=models.IntegerField(blank=True, choices=[(1, 'hombre'), (2, 'mujer'), (3, 'indeterminado'), (9, 'desconocido')], null=True),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='Telefono',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
