# -*- encoding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
url(r'^crear_antecedentes_h/(?P<paciente_id>[-\w]+)/$', views.crear_antecedentes_h, name='crear_antecedentes_h'),
url(r'^crear_antecedentes_h/$', views.crear_antecedentes_h, name='crear_antecedentes_h'),
url(r'^crear_antecedentes_m/(?P<paciente_id>[-\w]+)/$', views.crear_antecedentes_m, name='crear_antecedentes_m'),
url(r'^crear_antecedentes_m/$', views.crear_antecedentes_m, name='crear_antecedentes_m'),
#url(r'^detalle_antecedentes_m/$', views.detalle_antecedentes_m, name='detalle_antecedentes_m'),
url(r'^detalle_antecedentes_h/(?P<paciente_id>[-\w]+)/$', views.detalle_antecedentes_h, name='detalle_antecedentes_h'),

url(r'^detalle_antecedentes_m/(?P<paciente_id>[-\w]+)/$', views.detalle_antecedentes_m, name='detalle_antecedentes_m'),
url(r'^editar_antecedentes_h/(?P<paciente_id>[-\w]+)/$', views.editar_antecedentes_h, name='editar_antecedentes_h'),
url(r'^editar_antecedentes_m/(?P<paciente_id>[-\w]+)/$', views.editar_antecedentes_m, name='editar_antecedentes_m'),
]