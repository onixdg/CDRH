# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from hora.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import espermio_form, separacion_form, separacion_o_form, inseminacion_form
from hora.forms import hora_form
from django.shortcuts import get_object_or_404
# Create your views here.

#función listar_espermiogramas que se renderiza en template listar_espermio.html, lista todos los expermiogramas existentes
def listar_espermio(request):
	espermio = Espermiograma.objects.all()
	return render (request, "andrologia/listar_espermio.html", {'espermio': espermio})

#función listar_separacion que se renderiza en template listar_separacion.html, lista todos los separaciones existentes
def listar_separacion(request):
	separacion = Separacion.objects.all()
	return render (request, "andrologia/listar_separacion.html", {'separacion': separacion})

def listar_separacion_o(request):
	separacion = Separacion_o.objects.all()
	return render (request, "andrologia/listar_separacion_o.html", {'separacion': separacion})

def listar_inseminacion(request):
	inseminacion = Inseminacion.objects.all()
	return render (request, "andrologia/listar_inseminacion.html", {'inseminacion': inseminacion})

#función home_andrología que se renderiza en template home_andrologia.html, ejemplo de retorno sin contexto
def home_andrologia(request):
	request.session['rol'] = 'andrologia'
	examen = Examen.objects.all()
	return render (request, 'andrologia/home_andrologia.html', {'examen': examen })

#función crear_espermio que se renderiza en template crear_espermio.html, llenar campos faltantes
def seleccionar_paciente(request):
	paciente = Paciente.objects.all()
	return render (request, "andrologia/seleccionar_paciente.html", {'paciente': paciente})

def crear_espermio(request, examen_id=None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
	 	if request.method =='POST':
	 		form = espermio_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		
	 		#paciente = paciente_id
	 		form= espermio_form()
	 		form.fields['Info_examen'].initial = examen
	 		
	 	return render (request, 'andrologia/crear_espermio.html',{'form':form,'examen_id':examen_id})

def crear_separacion(request, examen_id=None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
	 	if request.method =='POST':
	 		form = separacion_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= separacion_form()
	 		form.fields['Info_examen'].initial = examen
	 		
	 	return render (request, 'andrologia/crear_separacion.html',{'form':form})

def crear_separacion_o(request, examen_id=None):
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
	 	if request.method =='POST':
	 		form = separacion_o_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= separacion_o_form()
	 		form.fields['Info_examen'].initial = examen
	 	return render (request, 'andrologia/crear_separacion_o.html',{'form':form,'examen_id':examen_id})

def crear_inseminacion(request,examen_id=None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
	 	if request.method =='POST':
	 		form = inseminacion_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= inseminacion_form()
	 		form.fields['Info_examen'].initial = examen
	 	return render (request, 'andrologia/crear_inseminacion.html',{'form':form})

def editar_examen(request,examen_id = None):
	examen = Examen.objects.get (id=examen_id)
	if request.method =='POST':
	 		form = hora_form(request.POST, instance = examen)
	 		if form.is_valid():
	 			form.save()
	 			return redirect('home_andrologia')
 
 	else:
 		form = hora_form(instance = examen)
	
	return render (request, 'andrologia/editar_examen.html/',{'form':form})

def detalle_espermio (request,examen_id = None):
	examen = get_object_or_404(Examen,pk=examen_id)
	espermio = get_object_or_404(Espermiograma,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_espermio.html/',{'espermio':espermio})

def detalle_separacion (request,examen_id = None):
	examen = get_object_or_404(Examen,pk=examen_id)
	separacion = get_object_or_404(Separacion,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_separacion.html/',{'separacion':separacion})

def detalle_separacion_o (request,examen_id = None):
	examen = get_object_or_404(Examen,pk=examen_id)
	separacion = get_object_or_404(Separacion_o,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_separacion_o.html/',{'separacion':separacion})	 	

def detalle_inseminacion (request,examen_id = None):
	examen = get_object_or_404(Examen,pk=examen_id)
	inseminacion = get_object_or_404(Inseminacion,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_inseminacion.html/',{'inseminacion':inseminacion})



def editar_espermio(request,examen_id = None):
	espermio = Espermiograma.objects.get (id =examen_id)
	if request.method =='POST':
	 		form = espermio_form(request.POST, instance = espermio)
	 		if form.is_valid():
	 			form.save()
	 			return redirect('listar_examen')
 
 	else:
 		form = espermio_form(instance = espermio)
	return render (request, 'andrologia/editar_espermio.html',{'form':form})



def editar_separacion (request, examen_id =None):
	separacion = Separacion.objects.get (id =examen_id)
	if request.method =='POST':
		form = separacion_form(request.POST, instance = separacion)
	 	if form.is_valid():
	 		form.save()
	 		return redirect('listar_examen')
	else:
 		form = separacion_form(instance = separacion)
 	return render (request, 'andrologia/editar_separacion.html/',{'form':form})

 

def editar_separacion_o (request, examen_id=None):
	separacion = Separacion_o.objects.get (id =examen_id)
	if request.method =='POST':
	 	form = separacion_o_form(request.POST, instance = separacion)
	 	if form.is_valid():
	 		form.save()
	 		return redirect('listar_examen')
	else:
 		form = separacion_o_form(instance = separacion)
 	return render (request, 'andrologia/editar_separacion_o.html/',{'form':form})


def editar_inseminacion (request, examen_id = None):
	inseminacion = Inseminacion.objects.get (id =examen_id)
	if request.method =='POST':
	 	form = inseminacion_form(request.POST, instance = inseminacion)
	 	if form.is_valid():
	 		form.save()
	 		return redirect('listar_examen')
	else:
 		form = inseminacion_form(instance = inseminacion)
 	return render (request, 'andrologia/editar_inseminacion.html/',{'form':form})