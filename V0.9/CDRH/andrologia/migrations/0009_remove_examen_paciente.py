# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-20 16:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('andrologia', '0008_auto_20160613_1644'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='examen',
            name='Paciente',
        ),
    ]
