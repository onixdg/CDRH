#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from paciente.models import Paciente as pac
from hora.models import Examen as exm
from andrologia.models import Espermiograma as esp
from andrologia.models import Separacion as se
from andrologia.models import Separacion_o as  so
from andrologia.models import Inseminacion as iiu
from django.utils import timezone
