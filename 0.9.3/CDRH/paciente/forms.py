  #!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from .models import Paciente
from django.core.exceptions import ValidationError

#def rut(num,dv):

			#ini=num
			#conta=2
			#suma=0
			#while num>0:
					#suma= suma + (conta * (num%10))
					#conta=conta+1
					#if conta==8:
						#conta=2
	                #num=num/10
			#conta=suma%11
			#valor=11-conta
			#if valor==10:
				#valor="K"	
	        
			#if valor==11:
			#	valor="0"
			#if (valor != int(dv)):
			#	return 1 
			#else:
			#	return 0
# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class paciente_form (forms.ModelForm):

	class Meta:
		model = Paciente
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'
		conyuge = Paciente.objects.all()
		widgets = {
			'RUN' : forms.TextInput(attrs={'size':8}),
            'DV' : forms.TextInput(attrs={'size':1}),
            'Codigo' : forms.TextInput(attrs={'size':3}),
            'Edad' : forms.TextInput(attrs={'size':3}),
            'Email' : forms.EmailInput(),
            'Fecha_de_nacimiento' : forms.DateInput(format=('%d-%m-%Y'),attrs={'class':'datepicker'}),
        
        }




	def clean_Nombres(self):
		diccionario_limpio = self.cleaned_data
		Nombres = diccionario_limpio.get("Nombres")
		if (Nombres == None or (len(Nombres)==0) ) :
				raise forms.ValidationError("Los Nombres son requeridos")
		return Nombres
	# Validación de campo PH, el ph sólo se mueve en el rango de 0 a 14
	def clean_Primer_apellido(self):
		diccionario_limpio = self.cleaned_data
		Primer_apellido = diccionario_limpio.get("Primer_apellido")
		if (Primer_apellido == None or (len(Primer_apellido)==0)) :
				raise forms.ValidationError("El apellido paterno es requerido")
		return Primer_apellido
	
	def clean_Segundo_apellido(self):
		diccionario_limpio = self.cleaned_data
		Segundo_apellido = diccionario_limpio.get("Segundo_apellido")
		if (Segundo_apellido == None or (len(Segundo_apellido)==0)) :
				raise forms.ValidationError("El apellido materno es requerido")
		return Segundo_apellido

	def clean_DV(self):
		diccionario_limpio = self.cleaned_data
		DV = diccionario_limpio.get("DV")
		if (DV == None or (len(str(DV))==0)) :
				raise forms.ValidationError("El dígito verificador es requerido")
		return DV

	def clean_RUN(self):
		diccionario_limpio = self.cleaned_data
		RUN = diccionario_limpio.get("RUN")
		validacion = Paciente.objects.filter(RUN = RUN)
		if (validacion) :
				raise forms.ValidationError("El rut ya existe")
		return RUN


	#def clean(self):
		#diccionario_limpio = self.cleaned_data
		#RUN = diccionario_limpio.get("RUN")
		#DV = diccionario_limpio.get("DV")

		#if (RUN and DV):
			#r = rut(RUN,DV)
			#if (r==1):
				#raise forms.RUN.ValidationError("El Rut no es valido")


	def clean_Sexo(self):
		diccionario_limpio = self.cleaned_data
		Sexo = diccionario_limpio.get("Sexo")
		if (Sexo == None or (len(str(Sexo))==0)) :
				raise forms.ValidationError("El sexo es requerido")
		return Sexo

	def clean_Edad(self):
		diccionario_limpio = self.cleaned_data
		Edad = diccionario_limpio.get("Edad")
		if (Edad == None or (len(str(Edad))==0)) :
				raise forms.ValidationError("La edad es requerida")
		if (Edad<=5) :
				raise forms.ValidationError("La edad no es valida")

		return Edad

	def clean_Fecha_de_nacimiento(self):
		diccionario_limpio = self.cleaned_data
		Fecha_de_nacimiento = diccionario_limpio.get("Fecha_de_nacimiento")
		if (Fecha_de_nacimiento == None or (len(str(Fecha_de_nacimiento))==0)) :
				raise forms.ValidationError("La fecha de nacimiento es requerida")
		return Fecha_de_nacimiento

	def clean_Email(self):
		diccionario_limpio = self.cleaned_data
		Email = diccionario_limpio.get("Email")
		if (Email == None or (len(str(Email))==0)) :
				raise forms.ValidationError("El email es requerido")
		return Email

	def clean_Telefono(self):
		diccionario_limpio = self.cleaned_data
		Telefono = diccionario_limpio.get("Telefono")
		if (Telefono == None or (len(str(Telefono))==0)) :
				raise forms.ValidationError("El telefono es requerido")
		return Telefono

	def clean_Direccion(self):
		diccionario_limpio = self.cleaned_data
		Direccion = diccionario_limpio.get("Direccion")
		if (Direccion == None or (len(str(Direccion))==0)) :
				raise forms.ValidationError("La direccion es requerida")
		return Direccion

	def clean_Ciudad(self):
		diccionario_limpio = self.cleaned_data
		Ciudad = diccionario_limpio.get("Ciudad")
		if (Ciudad == None or (len(str(Ciudad))==0)) :
				raise forms.ValidationError("La ciudad es requerida")
		return Ciudad

	def clean_Origen(self):
		diccionario_limpio = self.cleaned_data
		Origen = diccionario_limpio.get("Origen")
		if (Origen == None or (len(str(Origen))==0)) :
				raise forms.ValidationError("El origen es requerido")
		return Origen