#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from andrologia.models import *
from paciente.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import paciente_form
#función listar_espermiogramas que se renderiza en template listar_espermio.html, lista todos los expermiogramas existentes
def listar_paciente(request):
	paciente = Paciente.objects.all()
	return render (request, "paciente/listar_paciente.html", {'paciente': paciente})

# Create your views here.
def crear_paciente(request):
 	
	 	if request.method =='POST':
	 		form = paciente_form(request.POST)
	 		
	 		if form.is_valid():
	 			conyuge = form.cleaned_data.get('Conyuge')
	 			form.save()
	 			if (conyuge):
	 				Conyuge = Paciente.objects.get(id = conyuge.id)
	 				Pareja =  Paciente.objects.last()
	 				
	 				conyuge.Conyuge = Pareja
	 				conyuge.save()
	 			
	 			return redirect('listar_paciente')
	 	else:
	 		
	 		form= paciente_form()
	 	return render (request, 'paciente/crear_paciente.html',{'form':form})

def detalle_paciente (request, paciente_id):
	paciente = get_object_or_404(Paciente,pk=paciente_id)
	return render (request, 'paciente/detalle_paciente.html',{'paciente':paciente})

def editar_paciente(request,paciente_id = None):
	paciente = get_object_or_404(Paciente,pk=paciente_id)
	if request.method =='POST':
	 		form = paciente_form(request.POST, instance = paciente)
	 		if form.is_valid():
	 			form.save()
	 			return redirect('listar_paciente')
 
 	else:
 		form = paciente_form(instance = paciente)
	return render (request, 'paciente/editar_paciente.html',{'form':form,'paciente':paciente})
