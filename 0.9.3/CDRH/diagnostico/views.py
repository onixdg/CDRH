#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from forms import *
from diagnostico.models import *
from andrologia.models import *
from django.shortcuts import redirect
# Create your views here.


def crear_diagnostico_esp(request, espermio_id=None):
 	if (espermio_id):
 		espermio=Espermiograma.objects.get(id= espermio_id)
	if request.method =='POST':
	 	form = diag_esp_form(request.POST)
	 	if form.is_valid():
	 		espermio = form.cleaned_data.get('Espermiograma')
	 		form.save()
	 		return redirect('editar_espermio', examen_id=espermio.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_esp_form()
	 	form.fields['Espermiograma'].initial = espermio
	return render (request, 'diagnostico/crear_diagnostico_esp.html',{'form':form,'espermio':espermio})

def crear_diagnostico_se(request, separacion_id=None):
 	if (separacion_id):
 		separacion=Separacion.objects.get(id= separacion_id)
	if request.method =='POST':
	 	form = diag_se_form(request.POST)
	 	if form.is_valid():
	 		separacion = form.cleaned_data.get('Separacion')
	 		form.save()
	 		return redirect('editar_separacion', examen_id=separacion.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_se_form()
	 	form.fields['Separacion'].initial = separacion
	return render (request, 'diagnostico/crear_diagnostico_se.html',{'form':form,'separacion':separacion})

def crear_diagnostico_so(request, separacion_o_id=None):
 	if (separacion_o_id):
 		separacion_o=Separacion_o.objects.get(id= separacion_o_id)
	if request.method =='POST':
	 	form = diag_so_form(request.POST)
	 	if form.is_valid():
	 		separacion_o = form.cleaned_data.get('Separacion_o')
	 		form.save()
	 		return redirect('editar_separacion_o', examen_id=separacion_o.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_so_form()
	 	form.fields['Separacion_o'].initial = separacion_o
	return render (request, 'diagnostico/crear_diagnostico_so.html',{'form':form,'separacion_o':separacion_o})

def crear_diagnostico_iiu(request, inseminacion_id=None):
 	if (inseminacion_id):
 		inseminacion=Inseminacion.objects.get(id= inseminacion_id)
	if request.method =='POST':
	 	form = diag_iiu_form(request.POST)
	 	if form.is_valid():
	 		inseminacion = form.cleaned_data.get('Inseminacion')
	 		form.save()
	 		return redirect('editar_inseminacion', examen_id=inseminacion.id)
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form= diag_iiu_form()
	 	form.fields['Inseminacion'].initial = inseminacion
	return render (request, 'diagnostico/crear_diagnostico_iiu.html',{'form':form,'inseminacion':inseminacion})

def generar_diagnostico_esp(request,espermiograma_id=None):
	
	if (espermiograma_id):
		 espermiograma=Espermiograma.objects.get(id= espermiograma_id)
	#Oligozoospermia	
	if (espermiograma.Concentracion < 15):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='88311004')
			diagnostico.save() 
	#Astenozoospermia
	if (espermiograma.Progresivos <32 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='24463005')
			diagnostico.save() 
	#Teratozoospermia
	if (espermiograma.Normales <4 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='236817003')
			diagnostico.save()
	#Oligoastenozoospermia
	if (espermiograma.Concentracion < 15 and espermiograma.Progresivos <32 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='448532006')
			diagnostico.save()
	#Oligoteratozoospermia falta
	#if (espermiograma.Normales <4 and espermiograma.Progresivos <32 ):
		 	#diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='olite')
	#Oligoastenoteratozoospermia falta
	#if (espermiograma.Normales <4 and espermiograma.Progresivos <32 ):
		 	#diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='oliat')
	#Astenoteratozoospermia 
	if (espermiograma.Progresivos < 32 and espermiograma.Normales <4 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='astte')
			diagnostico.save()
	#Necrozoospermia
	if (espermiograma.Vitalidad <58 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='67708007')
			diagnostico.save()
	#Azoospermia revisar que hay muchos tipos de azospermia
	if (espermiograma.Concentracion == 0 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='48188009')
			diagnostico.save()
	#Hipospermia
	if (espermiograma.Volumen  < 1.5 ):
		 	diagnostico = Diagnostico_esp.objects.create(Espermiograma = espermiograma, Diagnostico='hip')
			diagnostico.save()		
	  		
	return redirect('listar_examen')

def generar_diagnostico_se(request,separacion_id=None):
	
	if (separacion_id):
		 separacion=Separacion.objects.get(id= separacion_id)
	#Oligozoospermia	
	if (separacion.Concentracion < 15):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='88311004')
			diagnostico.save() 
	#Astenozoospermia
	if (separacion.Progresivos <32 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='24463005')
			diagnostico.save() 
	#Teratozoospermia
	if (separacion.Normales <4 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='236817003')
			diagnostico.save()
	#Oligoastenozoospermia
	if (separacion.Concentracion < 15 and separacion.Progresivos <32 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='448532006')
			diagnostico.save()
	#Oligoteratozoospermia falta
	#if (separacion.Normales <4 and separacion.Progresivos <32 ):
		 	#diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='olite')
	#Oligoastenoteratozoospermia falta
	#if (separacion.Normales <4 and separacion.Progresivos <32 ):
		 	#diagnostico = Diagnostico_se.objects.create(Espermiograma = separacion, Diagnostico='oliat')
	#Astenoteratozoospermia 
	if (separacion.Progresivos < 32 and separacion.Normales <4 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='astte')
			diagnostico.save()
	#Necrozoospermia
	if (separacion.Vitalidad <58 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='67708007')
			diagnostico.save()
	#Azoospermia revisar que hay muchos tipos de azospermia
	if (separacion.Concentracion == 0 ):
			diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='48188009')
			diagnostico.save()
	#Hipospermia
	if (separacion.Volumen  < 1.5 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='hip')
			diagnostico.save()		
	  		
	return redirect('listar_examen')

def generar_diagnostico_se_o(separacion_id=None):
	
	if (separacion_id):
		 separacion=Separacion.objects.get(id= separacion_id)
	#Oligozoospermia	
	if (separacion.Concentracion < 15):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='88311004')
			diagnostico.save() 
	#Astenozoospermia
	if (separacion.Progresivos <32 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='24463005')
			diagnostico.save() 
	#Teratozoospermia
	if (separacion.Normales <4 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='236817003')
			diagnostico.save()
	#Oligoastenozoospermia
	if (separacion.Concentracion < 15 and separacion.Progresivos <32 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='448532006')
			diagnostico.save()
	#Oligoteratozoospermia falta
	#if (separacion.Normales <4 and separacion.Progresivos <32 ):
		 	#diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='olite')
	#Oligoastenoteratozoospermia falta
	#if (separacion.Normales <4 and separacion.Progresivos <32 ):
		 	#diagnostico = Diagnostico_se.objects.create(Espermiograma = separacion, Diagnostico='oliat')
	#Astenoteratozoospermia 
	if (separacion.Progresivos < 32 and separacion.Normales <4 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='astte')
			diagnostico.save()
	#Necrozoospermia
	if (separacion.Vitalidad <58 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='67708007')
			diagnostico.save()
	#Azoospermia revisar que hay muchos tipos de azospermia
	if (separacion.Concentracion == 0 ):
			diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='48188009')
			diagnostico.save()
	#Hipospermia
	if (separacion.Volumen  < 1.5 ):
		 	diagnostico = Diagnostico_se.objects.create(Separacion = separacion, Diagnostico='hip')
			diagnostico.save()		
	  		
	return redirect('listar_examen')


def eliminar_diagnostico_esp(request, diagnostico_id=None):
 	if (diagnostico_id):
 		diagnostico=Diagnostico_esp.objects.get(id= diagnostico_id)
 		espermiograma = Espermiograma.objects.get(id = diagnostico.Espermiograma.id)
 		diagnostico.delete()
	
	return redirect('editar_espermio', examen_id=espermiograma.id)

def eliminar_diagnostico_se(request, diagnostico_id=None):
 	if (diagnostico_id):
 		diagnostico=Diagnostico_se.objects.get(id= diagnostico_id)
 		separacion = Separacion.objects.get(id = diagnostico.Separacion.id)
 		diagnostico.delete()
	
	return redirect('editar_separacion', examen_id=separacion.id)

def eliminar_diagnostico_so(request, diagnostico_id=None):
 	if (diagnostico_id):
 		diagnostico=Diagnostico_so.objects.get(id= diagnostico_id)
 		separacion = Separacion_o.objects.get(id = diagnostico.Separacion_o.id)
 		diagnostico.delete()
	
	return redirect('editar_separacion_o', examen_id=separacion.id)

def eliminar_diagnostico_iiu(request, diagnostico_id=None):
 	if (diagnostico_id):
 		diagnostico=Diagnostico_iiu.objects.get(id= diagnostico_id)
 		inseminacion = Inseminacion.objects.get(id = diagnostico.Inseminacion.id)
 		diagnostico.delete()
	
	return redirect('editar_inseminacion', examen_id=inseminacion.id)
	
	
	 		
	 	