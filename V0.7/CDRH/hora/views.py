# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from hora.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import hora_form
# Create your views here.

# Create your views here.
def crear_hora(request):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 	
	 	if request.method =='POST':
	 		form = hora_form(request.POST)
	 		if form.is_valid():
	 			form.save()
	 			return redirect('home_secretaria')
	 	else:
	 		
	 		form= hora_form()
	 	return render (request, 'hora/crear_hora.html',{'form':form})