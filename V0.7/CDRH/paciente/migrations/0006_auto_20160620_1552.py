# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-20 15:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paciente', '0005_auto_20160617_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paciente',
            name='Origen',
            field=models.CharField(blank=True, choices=[('h', 'hospital Carlos Van Buren'), ('c', 'Centro De Reproducci\xf3n'), ('p', 'Particular')], max_length=1),
        ),
    ]
