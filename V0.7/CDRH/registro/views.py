# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from registro.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
#from forms import espermio_form

# Create your views here.
#función home_andrología que se renderiza en template home_andrologia.html, ejemplo de retorno sin contexto
def home_registro(request):
    #return HttpResponse("home_andrologia.html")
    return render (request, 'registro/home_registro.html', None)

def home_secretaria(request):
    #return HttpResponse("home_andrologia.html")
    return render (request, 'registro/home_secretaria.html', None)