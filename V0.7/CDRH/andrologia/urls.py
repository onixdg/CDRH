# -*- encoding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes
url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),
url(r'^listar_separacion/', views.listar_separacion, name='listar_separacion'),
url(r'^listar_separacion_o/', views.listar_separacion_o, name='listar_separacion_o'),
url(r'^listar_inseminacion/', views.listar_inseminacion, name='listar_inseminacion'),
# Template home de andrología debería mostrar las horas del día y el estado de la hora (en espera, atendido, no atendido), aun no se ha construido.
url(r'^home_andrologia/', views.home_andrologia, name='home_andrologia'),
url(r'^crear_espermio/(?P<examen_id>[-\w]+)/$', views.crear_espermio, name='crear_espermio'),
url(r'^crear_espermio/', views.crear_espermio, name='crear_espermio'),   
#url(r'^crear_espermio/(?P<pid>\d+)', 'CDRH.andrologia.views.crear_espermio', name='crear_espermio'),
#url(r'^crear_espermio/(?P<pid>.*)/$',views.crear_espermio, name='crear_espermio'),

url(r'^seleccionar_paciente/', views.seleccionar_paciente, name='seleccionar_paciente'), 
url(r'^crear_separacion/(?P<examen_id>[-\w]+)/$', views.crear_separacion, name='crear_separacion'),
url(r'^crear_separacion/', views.crear_separacion, name='crear_separacion'),
url(r'^crear_separacion_o/(?P<examen_id>[-\w]+)/$', views.crear_separacion_o, name='crear_separacion_o'),
url(r'^crear_separacion_o/', views.crear_separacion_o, name='crear_separacion_o'),

url(r'^crear_separacion_o/(?P<examen_id>[-\w]+)/$', views.crear_separacion_o, name='crear_separacion_o'),
url(r'^crear_inseminacion/', views.crear_inseminacion, name='crear_inseminacion'),
url(r'^crear_inseminacion/(?P<examen_id>[-\w]+)/$', views.crear_inseminacion, name='crear_inseminacion'),
url(r'^editar_examen/(?P<examen_id>[-\w]+)/$', views.editar_examen, name='editar_examen'),
url(r'^editar_examen/', views.editar_examen, name='editar_examen'),
]