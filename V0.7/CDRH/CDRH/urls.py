# -*- encoding: utf-8 -*-
"""CDRH URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
	url(r'^andrologia/', include('andrologia.urls')),
    url(r'^paciente/', include('paciente.urls')),
    url(r'^registro/', include('registro.urls')),
    url(r'^hora/', include('hora.urls')),
    url(r'^admin/', admin.site.urls),
]

# esto debería cambiarse en el ambiente de despliegue, codigo sólo utilizado en ambiente de desarrollo
if settings.DEBUG:
    urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)