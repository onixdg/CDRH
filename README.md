Release notes

Branches: 
Master: Versiones estables (baselist)
Desarrollo: Desarrollo de versiones Beta

Desarrollo:
v0.1 27 mayo 2016:
-Creación de clase espermiograma Models/espermiograma
-Configuración inicial Setting.py:
    -archivos estaticos
    -templates
    -string de conexión
-Adaptación de componentes Bootstrap
    -Css y Js en carpeta CDRH/static/framework
    -Uso de navbar en template base
-Generación de template base base.html
-Creación de funciones en views
    -views/listar_espermio, consulta y desplega todos los espertmiograma creados.
    -views/home_andrologia, lista el home (no construido
    -views/crear_espermio, pide el formulario y crea espermiograma.

v0.2 03 Junio 2016:
-Creación de clase Paciente Models/Paciente (desarrollado bajo la norma Chilena "estándares de identificación de la persona atendida", atributos comentados por no uso en el CDRH)
-Creación de clase Diagnostico Models/Diagnostico, hasta el momento sólo tiene implementado diagnostico para el examen de espermiograma.
-Creación de clase Examen, guarda los nosmbres, descripción, hora y estado de los examnetes. Sujeto a cambios a medida que avance el desarrollo
-Creación de formulario para espermiograma forms/espermio_form
    -Petición de todos los campos
    -Validación de campos desde el mismo codigo del formulario
-Mejora en template template/andrologia/crear_espermio , trabajo en etetica, ordenamiento de campos y trabajo en Css
    -Creación de CDRH.css para la declaración de estilos particulares 

