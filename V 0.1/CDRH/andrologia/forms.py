from django import forms
from .models import Espermiograma


# Create your forms here.
class espermio_form (forms.ModelForm):
	class Meta:
		model = Espermiograma
		fields = '__all__'
		