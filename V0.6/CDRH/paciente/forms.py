# -*- coding: UTF-8 -*-
from django import forms
from .models import Paciente
from django.core.exceptions import ValidationError


# Formulario espermiograma
# Formulario se renderiza en template "crear_espermio.html" 
class paciente_form (forms.ModelForm):
	class Meta:
		model = Paciente
		# Se hará petición de todos los campos del modelo, incluida claves foraneas 
		fields = '__all__'
		widgets = {
			'RUN' : forms.TextInput(attrs={'size':8}),
            'DV' : forms.TextInput(attrs={'size':1}),
            'Codigo' : forms.TextInput(attrs={'size':3}),
            'Edad' : forms.TextInput(attrs={'size':3}),
            'Email' : forms.EmailInput(),
            'Fecha_de_nacimiento' : forms.DateInput(format=('%d-%m-%Y'),attrs={'class':'datepicker'}),

        }
		