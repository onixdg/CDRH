# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-21 17:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paciente', '0005_auto_20160915_1543'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paciente',
            name='Sexo',
            field=models.IntegerField(blank=True, choices=[(1, 'hombre'), (2, 'mujer')], null=True),
        ),
    ]
