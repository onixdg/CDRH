#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from andrologia.models import *
from paciente.models import *
from hora.models import *
from diagnostico.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import espermio_form, separacion_form, separacion_o_form, inseminacion_form
from hora.forms import hora_form
from diagnostico.forms import *
from django.shortcuts import get_object_or_404
from datetime import * 

# Create your views here.

#función listar_espermiogramas que se renderiza en template listar_espermio.html, lista todos los expermiogramas existentes
def listar_espermio(request):
	espermio = Examen.objects.filter(Tipo = 'es').order_by('Fecha')
	return render (request, "andrologia/listar_espermio.html", {'espermio': espermio})

#función listar_separacion que se renderiza en template listar_separacion.html, lista todos los separaciones existentes
def listar_separacion(request):
	separacion = Examen.objects.filter(Tipo = 'se').order_by('Fecha')
	return render (request, "andrologia/listar_separacion.html", {'separacion': separacion})

def listar_separacion_o(request):
	separacion = Examen.objects.filter(Tipo = 'so').order_by('Fecha')
	return render (request, "andrologia/listar_separacion_o.html", {'separacion': separacion})

def listar_inseminacion(request):
	inseminacion = Examen.objects.filter(Tipo = 'iiu').order_by('Fecha')
	return render (request, "andrologia/listar_inseminacion.html", {'inseminacion': inseminacion})

def examen_completo(request):
	completo = Examen.objects.filter(Est_informe= 'c').order_by('Fecha')
	return render (request, "andrologia/examen_completo.html", {'completo': completo})

def examen_incompleto(request):
	incompleto = Examen.objects.filter(Est_informe= 'i').order_by('Fecha')
	return render (request, "andrologia/examen_incompleto.html", {'incompleto': incompleto})

def detalle_espermio (request,examen_id = None):
	
	espermio = get_object_or_404(Espermiograma,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_espermio.html/',{'espermio':espermio})

def detalle_separacion (request,examen_id = None):
	
	separacion = get_object_or_404(Separacion,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_separacion.html/',{'separacion':separacion})

def detalle_separacion_o (request,examen_id = None):
	
	separacion = get_object_or_404(Separacion_o,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_separacion_o.html/',{'separacion':separacion})	 	

def detalle_inseminacion (request,examen_id = None):
	
	inseminacion = get_object_or_404(Inseminacion,Info_examen=examen_id)
	return render (request, 'andrologia/detalle_inseminacion.html/',{'inseminacion':inseminacion})


#función home_andrología que se renderiza en template home_andrologia.html, ejemplo de retorno sin contexto
def home_andrologia(request):
	request.session['rol'] = 'andrologia'
	hoy = date.today()
	examen = Examen.objects.filter(Fecha__day = hoy.day, Fecha__month = hoy.month , Fecha__year = hoy.year).order_by('Fecha')
	#examen = Examen.objects.all()
	return render (request, 'andrologia/home_andrologia.html', {'examen': examen, 'hoy':hoy})

#función crear_espermio que se renderiza en template crear_espermio.html, llenar campos faltantes
def seleccionar_paciente(request):
	paciente = Paciente.objects.all()
	return render (request, "andrologia/seleccionar_paciente.html", {'paciente': paciente})

def crear_espermio(request, examen_id=None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
 			request.session["examen"] = examen.id
	 	if request.method =='POST':
	 		form = espermio_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			espermio =  Espermiograma.objects.last()
	 			estado = completitud_esp (espermio.id)
	 			if (estado == 1):	
	 				examen.Est_informe = "c"
	 			else:
	 				examen.Est_informe = "i"
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		
	 		#paciente = paciente_id
	 		form= espermio_form()
	 		examen_id = request.session["examen"]
	 		examen=Examen.objects.get(id= examen_id)
	 		form.fields['Info_examen'].initial = examen
	 	examen_id = request.session["examen"]
	 	examen=Examen.objects.get(id= examen_id)	
	 	return render (request, 'andrologia/crear_espermio.html',{'form':form,'examen':examen})

def crear_separacion(request, examen_id=None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
 			request.session["examen"] = examen.id
	 	if request.method =='POST':
	 		
	 		form = separacion_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			separacion =  Separacion.objects.last()
	 			estado = completitud_se (separacion.id)
	 			if (estado == 1):	
	 				examen.Est_informe = "c"
	 			else:
	 				examen.Est_informe = "i"
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= separacion_form()
	 		examen_id = request.session["examen"]
	 		examen=Examen.objects.get(id= examen_id)
	 		form.fields['Info_examen'].initial = examen
	 	examen_id = request.session["examen"]
	 	examen=Examen.objects.get(id= examen_id)
	 	return render (request, 'andrologia/crear_separacion.html',{'form':form,'examen':examen})

def crear_separacion_o(request, examen_id=None):
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
 			request.session["examen"] = examen.id
	 	if request.method =='POST':
	 		form = separacion_o_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			separacion =  Separacion_o.objects.last()
	 			estado = completitud_so (separacion.id)
	 			if (estado == 1):	
	 				examen.Est_informe = "c"
	 			else:
	 				examen.Est_informe = "i"
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= separacion_o_form()
	 		examen_id = request.session["examen"]
	 		examen=Examen.objects.get(id= examen_id)
	 		form.fields['Info_examen'].initial = examen
	 	examen_id = request.session["examen"]
	 	examen=Examen.objects.get(id= examen_id)
	 	return render (request, 'andrologia/crear_separacion_o.html',{'form':form,'examen':examen})

def crear_inseminacion(request,examen_id=None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (examen_id):
 			examen=Examen.objects.get(id= examen_id)
 			request.session["examen"] = examen.id
	 	if request.method =='POST':
	 		form = inseminacion_form(request.POST)
	 		if form.is_valid():
	 			examen = form.cleaned_data.get('Info_examen')
	 			form.save()
	 			inseminacion =  Inseminacion.objects.last()
	 			estado = completitud_iiu (inseminacion.id)

	 			if (estado == 1):	
	 				examen.Est_informe = "c"
	 			else:
	 				examen.Est_informe = "i"
	 			examen.Est_hora = 'a'
	 			examen.save()
	 			return redirect('listar_examen')
	 	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 		form= inseminacion_form()
	 		examen_id = request.session["examen"]
	 		examen=Examen.objects.get(id= examen_id)
	 		form.fields['Info_examen'].initial = examen
	 	examen_id = request.session["examen"]
	 	examen=Examen.objects.get(id= examen_id)
	 	return render (request, 'andrologia/crear_inseminacion.html',{'form':form,'examen':examen})

def editar_examen(request,examen_id = None):
	examen = Examen.objects.get (id=examen_id)
	if request.method =='POST':
	 		form = hora_form(request.POST, instance = examen)
	 		if form.is_valid():
	 			form.save()
	 			return redirect('home_andrologia')
 
 	else:
 		form = hora_form(instance = examen)
	
	return render (request, 'andrologia/editar_examen.html/',{'form':form})



def editar_espermio(request,examen_id = None):
	espermio = Espermiograma.objects.get (id =examen_id)
	if request.method =='POST':
	 		form = espermio_form(request.POST, instance = espermio)
	 		if form.is_valid():
	 			form.save()
	 			estado = completitud_esp (espermio.id)
	 			examen = form.cleaned_data.get('Info_examen')
	 			exa = Examen.objects.get (id = examen.id)
	 			if (estado == 1):	
	 				exa.Est_informe = "c"
	 			else:
	 				exa.Est_informe = "i"
	 			exa.save()
	 			return redirect('listar_examen')
 
 	else:
 		form = espermio_form(instance = espermio)
		
	return render (request, 'andrologia/editar_espermio.html',{'form':form, 'espermio': espermio})


def editar_separacion (request, examen_id =None):
	separacion = Separacion.objects.get (id =examen_id)
	if request.method =='POST':
		form = separacion_form(request.POST, instance = separacion)
	 	if form.is_valid():
	 		form.save()
	 		estado = completitud_se (separacion.id)
	 		examen = form.cleaned_data.get('Info_examen')
	 		exa = Examen.objects.get (id = examen.id)
	 		if (estado == 1):	
	 			exa.Est_informe = "c"
	 		else:
	 			exa.Est_informe = "i"
	 		exa.save()
	 		return redirect('listar_examen')
	else:
 		form = separacion_form(instance = separacion)
 	return render (request, 'andrologia/editar_separacion.html/',{'form':form,'separacion': separacion})

 

def editar_separacion_o (request, examen_id=None):
	separacion_o = Separacion_o.objects.get (id =examen_id)
	if request.method =='POST':
	 	form = separacion_o_form(request.POST, instance = separacion_o)
	 	if form.is_valid():
	 		form.save()
	 		estado = completitud_so (separacion_o.id)
	 		examen = form.cleaned_data.get('Info_examen')
	 		exa = Examen.objects.get (id = examen.id)
	 		if (estado == 1):	
	 			exa.Est_informe = "c"
	 		else:
	 			exa.Est_informe = "i"
	 		exa.save()
	 		return redirect('listar_examen')
	else:
 		form = separacion_o_form(instance = separacion_o)
 	return render (request, 'andrologia/editar_separacion_o.html/',{'form':form, 'separacion_o': separacion_o})


def editar_inseminacion (request, examen_id = None):
	inseminacion = Inseminacion.objects.get (id =examen_id)
	if request.method =='POST':
	 	form = inseminacion_form(request.POST, instance = inseminacion)
	 	if form.is_valid():
	 		form.save()
	 		estado = completitud_iiu (inseminacion.id)
	 		examen = form.cleaned_data.get('Info_examen')
	 		exa = Examen.objects.get (id = examen.id)
	 		if (estado == 1):	
	 			exa.Est_informe = "c"
	 		else:
	 			exa.Est_informe = "i"
	 		exa.save()
	 		return redirect('listar_examen')
	else:
 		form = inseminacion_form(instance = inseminacion)
 	return render (request, 'andrologia/editar_inseminacion.html/',{'form':form,'inseminacion': inseminacion})

# funciones que ayudan a cambiar el estado de los informes de androlía  ()
def completitud_esp (examen_id):
	i = Espermiograma.objects.get (id =examen_id)
	j=1
	if ((i.Tiempo_licu == None or (len(str(i.Tiempo_licu))==0)) or (i.Volumen == None or (len(str(i.Volumen))==0)) or (i.Aspecto == None or (len(str(i.Aspecto))==0)) or (i.Color == None or (len(str(i.Color))==0)) or (i.PH == None or (len(str(i.PH))==0)) or (i.Viscosidad == None or (len(str(i.Viscosidad))==0)) or (i.Cuerpos_gelatinosos == None or (len(str(i.Cuerpos_gelatinosos))==0)) or (i.Cuerpos_en_suspen == None or (len(str(i.Cuerpos_en_suspen))==0)) or (i.Cuerpos_mucosos == None or (len(str(i.Cuerpos_mucosos))==0)) or (i.Concentracion == None or (len(str(i.Concentracion))==0)) or (i.Vitalidad == None or (len(str(i.Vitalidad))==0)) or (i.Celulas_redondas == None or (len(str(i.Celulas_redondas))==0)) or (i.Detritus == None or (len(str(i.Detritus))==0)) or (i.Celulas_escamosas == None or (len(str(i.Celulas_escamosas))==0)) or (i.Aglutinacion == None or (len(str(i.Aglutinacion))==0)) or (i.Pseudoaglutinacion == None or (len(str(i.Pseudoaglutinacion))==0)) or (i.Cristales == None or (len(str(i.Cristales))==0)) or (i.Mucus == None or (len(str(i.Mucus))==0)) or (i.Bacterias == None or (len(str(i.Bacterias))==0)) or (i.Progresivos == None or (len(str(i.Progresivos))==0)) or (i.No_progresivos == None or (len(str(i.No_progresivos))==0)) or (i.Inmoviles == None or (len(str(i.Inmoviles))==0)) or (i.Normales == None or (len(str(i.Normales))==0)) or (i.Anormales == None or (len(str(i.Anormales))==0)) or (i.Defecto_cabeza == None or (len(str(i.Defecto_cabeza))==0)) or (i.Defecto_pieza == None or (len(str(i.Defecto_pieza))==0)) or (i.Defecto_cola == None or (len(str(i.Defecto_cola))==0)) or (i.Total == None or (len(str(i.Total))==0)) or (i.TEM == None or (len(str(i.TEM))==0))) :
		j=0
	return j

def completitud_se (examen_id):
	i = Separacion.objects.get (id =examen_id)
	j=1
	if ((i.Protocolo == None or (len(str(i.Protocolo))==0)) or (i.Tiempo_licu == None or (len(str(i.Tiempo_licu))==0)) or (i.Volumen == None or (len(str(i.Volumen))==0)) or (i.Volumen_post == None or (len(str(i.Volumen_post))==0)) or (i.Aspecto == None or (len(str(i.Aspecto))==0)) or (i.Color == None or (len(str(i.Color))==0)) or (i.PH == None or (len(str(i.PH))==0)) or (i.Viscosidad == None or (len(str(i.Viscosidad))==0)) or (i.Cuerpos_gelatinosos == None or (len(str(i.Cuerpos_gelatinosos))==0)) or (i.Cuerpos_en_suspen == None or (len(str(i.Cuerpos_en_suspen))==0)) or (i.Cuerpos_mucosos == None or (len(str(i.Cuerpos_mucosos))==0)) or (i.Concentracion == None or (len(str(i.Concentracion))==0)) or (i.Vitalidad == None or (len(str(i.Vitalidad))==0)) or (i.Celulas_redondas == None or (len(str(i.Celulas_redondas))==0)) or (i.Detritus == None or (len(str(i.Detritus))==0)) or (i.Celulas_escamosas == None or (len(str(i.Celulas_escamosas))==0)) or (i.Aglutinacion == None or (len(str(i.Aglutinacion))==0)) or (i.Pseudoaglutinacion == None or (len(str(i.Pseudoaglutinacion))==0)) or (i.Cristales == None or (len(str(i.Cristales))==0)) or (i.Mucus == None or (len(str(i.Mucus))==0)) or (i.Bacterias == None or (len(str(i.Bacterias))==0)) or (i.Progresivos == None or (len(str(i.Progresivos))==0)) or (i.No_progresivos == None or (len(str(i.No_progresivos))==0)) or (i.Inmoviles == None or (len(str(i.Inmoviles))==0)) or (i.Normales == None or (len(str(i.Normales))==0)) or (i.Anormales == None or (len(str(i.Anormales))==0)) or (i.Defecto_cabeza == None or (len(str(i.Defecto_cabeza))==0)) or (i.Defecto_pieza == None or (len(str(i.Defecto_pieza))==0)) or (i.Defecto_cola == None or (len(str(i.Defecto_cola))==0)) or (i.Total == None or (len(str(i.Total))==0)) or (i.TEM == None or (len(str(i.TEM))==0)) or (i.Concentracion_post == None or (len(str(i.Concentracion_post))==0)) or (i.Progresivos_post == None or (len(str(i.Progresivos_post))==0)) or (i.No_progresivos_post == None or (len(str(i.No_progresivos_post))==0)) or (i.Inmoviles_post == None or (len(str(i.Inmoviles_post))==0)) or (i.TEM_post == None or (len(str(i.TEM_post))==0))) :
		j=0
	return j

def completitud_so (examen_id):
	i = Separacion_o.objects.get (id =examen_id)
	j=1
	if ((i.Bicarbonato == None or (len(str(i.Bicarbonato))==0)) or (i.Volumen == None or (len(str(i.Volumen))==0)) or  (i.Volumen_post == None or (len(str(i.Volumen_post))==0)) or (i.Aspecto == None or (len(str(i.Aspecto))==0)) or  (i.Color == None or (len(str(i.Color))==0))or (i.PH == None or (len(str(i.PH))==0))  or (i.Progresivos == None or (len(str(i.Progresivos))==0)) or (i.No_progresivos == None or (len(str(i.No_progresivos))==0)) or (i.Inmoviles == None or (len(str(i.Inmoviles))==0)) or (i.Normales == None or (len(str(i.Normales))==0)) or (i.Anormales == None or (len(str(i.Anormales))==0)) or (i.Anormales == None or (len(str(i.Anormales))==0)) or (i.Defecto_cabeza == None or (len(str(i.Defecto_cabeza))==0)) or (i.Defecto_pieza == None or (len(str(i.Defecto_pieza))==0)) or (i.Defecto_cola == None or (len(str(i.Defecto_cola))==0)) or (i.Total == None or (len(str(i.Total))==0)) or (i.TEM == None or (len(str(i.TEM))==0)) or (i.Concentracion_post == None or (len(str(i.Concentracion_post))==0)) or (i.Progresivos_post == None or (len(str(i.Progresivos_post))==0)) or (i.No_progresivos_post == None or (len(str(i.No_progresivos_post))==0)) or (i.Inmoviles_post == None or (len(str(i.Inmoviles_post))==0))or (i.TEM_post == None or (len(str(i.TEM_post))==0))) :
		j=0
	return j

def completitud_iiu (examen_id):
	i = Inseminacion.objects.get (id =examen_id)
	j=1
	if ((i.Protocolo == None or (len(str(i.Protocolo))==0)) or (i.Concentracion == None or (len(str(i.Concentracion))==0)) or (i.Concentracion_post == None or (len(str(i.Concentracion_post))==0)) or (i.Volumen == None or (len(str(i.Volumen))==0)) or (i.Total == None or (len(str(i.Total))==0)) or (i.Progresivos == None or (len(str(i.Progresivos))==0)) or (i.TEM == None or (len(str(i.TEM))==0)) or (i.Volumen_post == None or (len(str(i.Volumen_post))==0)) or (i.Total_post == None or (len(str(i.Total_post))==0)) or (i.Progresivos_post == None or (len(str(i.Progresivos_post))==0)) or (i.TEM_post == None or (len(str(i.TEM_post))==0))) :
		j=0
	return j