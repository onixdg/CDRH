#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes
#url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),
url(r'^crear_diagnostico_esp/(?P<espermio_id>[-\w]+)/$', views.crear_diagnostico_esp, name='crear_diagnostico_esp'),
url(r'^crear_diagnostico_esp/$', views.crear_diagnostico_esp, name='crear_diagnostico_esp'),
#url(r'^crear_inseminacion/', views.crear_inseminacion, name='crear_inseminacion'),
url(r'^crear_diagnostico_se/(?P<separacion_id>[-\w]+)/$', views.crear_diagnostico_se, name='crear_diagnostico_se'),
url(r'^crear_diagnostico_so/(?P<separacion_o_id>[-\w]+)/$', views.crear_diagnostico_so, name='crear_diagnostico_so'),
url(r'^crear_diagnostico_iiu/(?P<inseminacion_id>[-\w]+)/$', views.crear_diagnostico_iiu, name='crear_diagnostico_iiu'),
url(r'^generar_diagnostico_esp/(?P<espermiograma_id>[-\w]+)/$', views.generar_diagnostico_esp, name='generar_diagnostico_esp'),
url(r'^generar_diagnostico_se/(?P<separacion_id>[-\w]+)/$', views.generar_diagnostico_se, name='generar_diagnostico_se'),
url(r'^eliminar_diagnostico_esp/(?P<diagnostico_id>[-\w]+)/$', views.eliminar_diagnostico_esp, name='eliminar_diagnostico_esp'),
url(r'^eliminar_diagnostico_se/(?P<diagnostico_id>[-\w]+)/$', views.eliminar_diagnostico_se, name='eliminar_diagnostico_se'),
url(r'^eliminar_diagnostico_so/(?P<diagnostico_id>[-\w]+)/$', views.eliminar_diagnostico_so, name='eliminar_diagnostico_so'),
url(r'^eliminar_diagnostico_iiu/(?P<diagnostico_id>[-\w]+)/$', views.eliminar_diagnostico_iiu, name='eliminar_diagnostico_iiu'),
]