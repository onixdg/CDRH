#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes
#url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),
url(r'^crear_hora/', views.crear_hora, name='crear_hora'),
url(r'^crear_hora_a/', views.crear_hora_a, name='crear_hora_a'),
url(r'^listar_examen/', views.listar_examen, name='listar_examen'),
#url(r'^crear_inseminacion/', views.crear_inseminacion, name='crear_inseminacion'),
]