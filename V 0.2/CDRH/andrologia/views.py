from django.shortcuts import render, redirect

from andrologia.models import Espermiograma
from django.http import HttpResponse
from forms import espermio_form
# Create your views here.
#listar espermiogramas
def listar_espermio(request):
	espermio = Espermiograma.objects.all()
	return render (request, "andrologia/listar_espermio.html", {'espermio': espermio})

def home_andrologia(request):

    #return HttpResponse("home_andrologia.html")
    return render (request, 'andrologia/home_andrologia.html', None)

def crear_espermio(request):
 	if request.method =='POST':
 		form = espermio_form(request.POST)
 		if form.is_valid():
 			form.save()
 			return redirect('listar_espermio')
 	else:
 		form= espermio_form()
 	return render (request, 'andrologia/crear_espermio.html',{'form':form,})

