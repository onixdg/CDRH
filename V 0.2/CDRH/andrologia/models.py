# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
#restringir camopos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator


#------------------------------------------------------------------------------------- CLASE PACIENTE
#desarrollado bajo la norma Chilena " estándares de identificación de la persona atendida"
class Paciente (models.Model):
	#sexo Y Previsión son opciones numericas según la norma de salud
	#SEXO = (
		#(1,'hombre'),
		#(2,'mujer',
		#(3,'indeterminado'),
		#(9,'desconocido'),
		#)
	#ESTADO_CONYUGAL = (
		#(1,'Soltero (a)'),
		#(2,'Casado (a)',
		#(3,'Viudo (a)'),
		#(4,'Separado (a)'),
		#(5,'Conviviente (a)'),
		#(6,'Divorciado (a)),
		#(9,'Desconocido'),
		#)
			
	#PREVISION = (
		#(1,'FONASA'),
		#(2,'Isapre',
		#(3,'Sin previsión'),
		#(5,'CAPREDENA'),
		#(6,'DIPRECA),
		#(7,'Otra'),
		#(9,'Ignorado'),
		#)
	#CLASIFICACION_BENEFICIARIO_FONASA = (A,B,C,D)

	#LEYES_SOCIALES = (
		#(1,'Ley 18.490 Accidentes de transporte'),
		#(2,'LEY 16.744 Accidentes del trabajo y Enfermedades Profesionales',
		#(3,'Ley 16.744 Accidente Escolar'),
		#(4,'Ley 19.650/99 de Urgencia'),
		#(5,'PRAIS'),
		#(6,'Chile Solidario),
		#(7,'Chile Crece Conmigo'),
		#(8,'Otra Programa social'),
		#(9,'GES'),
		#)

	#NIVEL_INSTRUCCION = (
		#('01','Sala cuna'),
		#('02','Educación parvularia',
		#('03','Educación básica'),
		#('04','Educación media científica humanista'),
		#('05','Educación media técnico profesional'),
		#('06','Humanidades (sistema antiguo)'),
		#('08','Chile Crece Conmigo'),
		#('10','Ley 16.744 Accidente Escolar'),
		#('12','Educación universitaria'),
		#('13','Postgrado'),
		#('14','Educación especial'),
		#('15','Ninguno'),
		#)
	#ESTANDAR_ABREVIADO_NIVEL_INSTRUCCION = (
		#('1','Pre-básica'),
		#('2','Básica (incluye educación especial /diferencial)',
		#('3','Media'),
		#('4','Técnica de nivel superior'),
		#('5','Superior'),
		#('6','Ninguno'),
		#)
	Nombres = models.CharField(max_length = 50,blank = True)
	Primer_apellido = models.CharField("Apellido paterno",max_length = 30,blank = True)
	Segundo_apellido = models.CharField("Apellido materno",max_length = 30,blank = True)
	#ojo con el validador de rut
	RUN = models.IntegerField("RUT",validators=[MaxValueValidator(10)],blank=True)
	DV = models.CharField(max_length=1,blank=True)
	#Edad = models.IntergerField(dígitos máximo 3)
	#Sexo = models.IntergerField(dígito maximo =1,choices=SEXO)
	#Estado_conyugal = models.IntergerField(dígito maximo =1,choices= ESTADO_CONYUGAL)
	#Prevision = models.Charfield("previsión",digito maximo = 1, choices = PREVISION )
	#Clasificacion_Beneficiario_FONASA = models.CharField("Clasificación beneficiario FONASA", max_length =1,null =true)
	#Leyes_sociales = models.IntergerField(dígito maximo =1,choices=LEYES_SOCIALES) 
	#def definir_clasificacion_fonada(self):
		#if self.presision == 1: deberia poner la CLASIFICACION_BENEFICIARIO_FONASA = (A,B,C,D)
	#Nivel_de_instrucción= models.Charfield("Nivel de intrucción",max_length =2,null = true ,choices = NIVEL_INSTRUCCION)
	#Estandar_abreviado_nivel_de_instrucción= model.Charfield("Estándar Abreviado Nivel de Instrucción",max_length =1,null = true ,choices = ESTANDAR_ABREVIADO_NIVEL_INSTRUCCION)
	#Ocupacion = models.Charfield ("Ocupación",max_length = 4, fuente CIUO-88)
	#Actividad_economica = models.Charfield ("Actividad Económiva",max_length = 6, fuente CIIU)
	#Nacionalidad
#------------------------------------------------------------------------------------- CLASE EXAMEN
class Examen(models.Model):
	TIPO = (
		('es' , 'Espermiograma'),
		('eso' , 'Espermiograma en orina'),
		('se' , 'Separación espermatica'),
		('iiu' , 'Inseminación intra uterina'),
		('ce' , 'Criopreservación de espermios'),
		('sa' , 'Separación de ADN'),
		)
	EST_INFORME = (
		('i' , 'incompleto'),
		('c' , 'completo'),
		)	
	EST_HORA = (
		('a' , 'atendido'),
		('e' , 'en espera'),
		('n' , 'no atendido'),
		)	
	Tipo = models.CharField(max_length = 3,blank = True,choices = TIPO)
	Fecha = models.DateTimeField(auto_now_add=True)
	Est_informe= models.CharField(max_length = 3,blank = True,choices = EST_INFORME)
	Est_hora= models.CharField(max_length = 3,blank = True,choices = EST_HORA)

	


#------------------------------------------------------------------------------------- CLASE DIAGNOSTICO
class Diagnostico(models.Model):
	Info_examen = models.ForeignKey(Examen)
	DIAGNOSTICO = (
		('olizo','Oligozoospermia'),
		('astzo','Astenozoospermia'),
		('ter','Teratozoospermia'),
		('olias','Oligoastenozoospermia'),
		('olite','Oligoteratozoospermia'),
		('oliat','Oligoastenoteratozoospermia'),
		('astte','Astenoteratozoospermia'),
		('nec','Necrozoospermia'), 
		('azo','Azoospermia'),
		('hip','Hipospermia'),
		('asp','Aspermia'),
		('leu','Leucocitospermia'),
		('hem','Hematospermia'),
		)
	Diagnostico = models.CharField("Diagnóstico",max_length = 5,blank = True,choices = DIAGNOSTICO)
	Comentario = models.TextField()


#------------------------------------------------------------------------------------- CLASE ESPERMIOGRAMA
#Clase enfocada a los detalles del informe del examen de tipo espermiograma
class Espermiograma(models.Model):
	Info_examen = models.ForeignKey(Examen)
	#Paciente = models.ForeignKey(Paciente)
	#Hora = models.ForeignKey(Hora)

	#rango numérico del tiempo de licufaccion 
	#TIEMPO_LICU = (15,20,25,30,35,40,45,50,55,60)
	#opciones de aspecto de la muestra
	ASPECTO = (
		('ho' , 'homogéneo'),
		('he' , 'heterogéneo'),
		('hof' , 'homogéneo filante'),
		('hef' , 'heterogéneo filante'),
		('t' , 'traslúcido'),
		('e' , 'espumoso'),
		)
	#opciones de color de la muestra
	COLOR = (
		('n' , 'normal'),
		('a' , 'amarillento'),
		('b' , 'blanquecino'),
		('r' , 'rojizo'),
		)
	#Viscosidad presente en la muestra
	VISCOSIDAD = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)	
	#Cuerpos gelatinoso presentes en la muestra
	C_GELATINOSOS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Cuerpos en suspención presentes en la muestra
	C_EN_SUSPEN = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Cuerpos en suspención presentes en la muestra
	C_MUCOSOS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Detritus
	DETRITUS = (
		('n' , 'negativo'),
		('l' , 'leve'),
		('m' , 'moderado'),
		('a' , 'abundante'),
		)
	#Células escamosas
	C_ESCAMOSAS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	AGLUTINACION = (
		('n' , 'negativo'),
		('coc' , 'cola-cola'),
		('cac' , 'cabeza-cabeza'),
		('coca' , 'cola-cabeza'),
		)
	PSEUDOAGLUTINACION = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	CRISTALES = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	MUCUS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	BACTERIAS = (
		('+' , 'positivo'),
		('-' , 'negativo'),
		)
	#Guarda la hora en que se finaliza informe
	#Hora_termino = models.DateTimeField(auto_now_add=True)
	#-------------------------------EXAMEN MACROSCÓPICO---------------------------------------
	Tiempo_licu = models.IntegerField("Tiempo Licuefacción",blank = True,null = True)
	Volumen = models.DecimalField(blank = True,null = True,max_digits=3, decimal_places=1) #recordar poner ml en el template
	Aspecto = models.CharField(max_length = 3,blank = True,null= True,choices = ASPECTO)
	Color = models.CharField(max_length = 1,blank = True,null = True,choices = COLOR)
	PH = models.FloatField(blank = True,null= True)
	Viscosidad = models.CharField(max_length = 1,blank = True,null = True,choices = VISCOSIDAD)
	Cuerpos_gelatinosos = models.CharField(max_length = 1,blank = True,null = True,choices = C_GELATINOSOS)
	Cuerpos_en_suspen = models.CharField("Cuerpos en suspensión",max_length = 1,blank = True,null = True,choices = C_EN_SUSPEN)
	Cuerpos_mucosos = models.CharField(max_length = 1,blank = True,null = True,choices = C_MUCOSOS)
	#-------------------------------EXAMEN MICROSCÓPICO---------------------------------------
	#concentración espermatica
	Concentracion = models.IntegerField("CONCENTRACIÓN ESPERMÁTICA",blank = True,null = True) #recordar poner en template X10⁶/ml
	Vitalidad = models.IntegerField("VITALIDAD",blank = True,null= True) #recordar poner el % en el template. (este valor se expresa en porcentaje)
	Celulas_redondas = models.IntegerField("Células redondas",blank = True,null = True) #puede ser negativo o un valor, Células
	Detritus = models.CharField(max_length = 1,blank = True,null = True,choices = DETRITUS)
	Celulas_escamosas = models.CharField("Células escamosas",max_length = 1,blank = True,null = True,choices = C_ESCAMOSAS)#Células escamosas
	Aglutinacion = models.CharField("Aglutinación",max_length = 4,blank = True,null = True,choices = AGLUTINACION)
	Pseudoaglutinacion = models.CharField("Pseudoaglutinación",max_length = 1,blank = True,null = True,choices = PSEUDOAGLUTINACION)
	Cristales = models.CharField(max_length = 1,blank = True,null = True,choices = CRISTALES)
	Mucus = models.CharField(max_length = 1,blank = True,null = True,choices=MUCUS)
	Bacterias = models.CharField(max_length = 1,blank = True,null = True,choices=BACTERIAS)
	#--------------------------------------MOTILIDAD-------------------------------------------
	Progresivos = models.IntegerField(blank = True,null = True) #recordar que es un %
	No_progresivos = models.IntegerField(blank=True,null = True) #recordar que es un %
	Inmoviles = models.IntegerField("Inmóviles",blank = True,null = True) # Inmóviles, recordar que es un %
	#--------------------------------------MORFOLOGÍA-------------------------------------------
	Normales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Anormales = models.IntegerField(blank = True,null = True) #recordar que es un %
	Defecto_cabeza = models.IntegerField(blank = True,null = True)
	Defecto_pieza = models.IntegerField(blank = True,null = True)
	Defecto_cola = models.IntegerField(blank = True,null = True)
		#----------------Total espermatozoides progresivos (TEM) en la muestra seminal:---------------------
	Total = models.IntegerField("RECUENTO TOTAL",blank = True,null = True) #[valor]X10⁶

	#Metodos de la clase Espermiograma
	def recuento_total(self):
		recuento = self.Concentracion * self.Volumen
		return recuento #Recordar que el recuento va con X10⁶ en el template
	#def valor_cr(self):
		#if (self.Celulas_redondas == Null):
			# Celulas_redondas = -1


#	"""docstring for ClassName"""
#	def __init__(self, arg):
#		super(ClassName, self).__init__()
#		self.arg = arg

