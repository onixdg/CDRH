from django.conf.urls import url

from . import views


urlpatterns = [
url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),
url(r'^home_andrologia/', views.home_andrologia, name='home_andrologia'),
url(r'^crear_espermio/', views.crear_espermio, name='crear_espermio'),   
]