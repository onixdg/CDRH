# -*- coding: UTF-8 -*-
from django import forms
from .models import Espermiograma
from django.core.exceptions import ValidationError


# Create your forms here.
class espermio_form (forms.ModelForm):
	class Meta:
		model = Espermiograma
		fields = '__all__'

	def clean_Tiempo_licu(self):
		diccionario_limpio = self.cleaned_data
		tiempo_licu = diccionario_limpio.get("Tiempo_licu")
		if (tiempo_licu != None) :
			if (int(tiempo_licu) > 60 or int(tiempo_licu) < 0):
				raise forms.ValidationError("El tiempo no puede ser menor a 0 min o mayor a los 60 min")
		return tiempo_licu

	def clean_PH(self):
		diccionario_limpio = self.cleaned_data
		ph = diccionario_limpio.get("PH")
		if (int(ph) > 14 or int(ph) < 0):
			raise forms.ValidationError("El PH se mueve en un rango de 0 a 14")
		return ph
		
		