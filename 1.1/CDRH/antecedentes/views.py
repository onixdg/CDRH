#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from andrologia.models import *
from paciente.models import *
from hora.models import *
from antecedentes.models import *
from django.http import HttpResponse #revisar si esta importacion es necesaría, no se esta haciendo uso de HttpResponse
from forms import *


def crear_antecedentes_h(request, paciente_id =None):
 	if (paciente_id):
 		paciente=Paciente.objects.get(pk= paciente_id)
	if request.method =='POST':
	 	form_h = habitos_form(request.POST)		
	 	form_m = medicos_form(request.POST)
	 	form_t = testiculares_form(request.POST)
	 	if form_h.is_valid() and form_m.is_valid() and form_t.is_valid():
	 		paciente = form_h.cleaned_data.get('Paciente') 
	 		form_h.save()
	 		form_m.save()
	 		form_t.save()
	 		return redirect('listar_paciente')
	else:
	 	#paciente=Paciente.objects.get(pk= 1)
	 	#paciente = paciente_id
	 	form_h= habitos_form()	
	 	form_m = medicos_form()	
	 	form_h.fields['Paciente'].initial = paciente
	 	form_m.fields['Paciente'].initial = paciente
	 	form_t = testiculares_form()	
	 	form_t.fields['Paciente'].initial = paciente
	return render (request, 'antecedentes/crear_antecedentes_h.html',{'form_h':form_h, 'form_m':form_m ,'form_t':form_t})

def crear_antecedentes_m(request, paciente_id =None):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
 		if (paciente_id):
 				paciente=Paciente.objects.get(pk= paciente_id)
	 	if request.method =='POST':
	 		form_h = habitos_form(request.POST)		
	 		form_m = medicos_form(request.POST)
	 		if form_h.is_valid() and form_m.is_valid():
	 			paciente = form_h.cleaned_data.get('Paciente') 
	 			form_h.save()
	 			form_m.save()
	 			return redirect('listar_paciente')
	 	else:
	 		paciente = get_object_or_404(Paciente,pk=paciente_id)
	 		#paciente = paciente_id
	 		form_h= habitos_form()	
	 		form_m = medicos_form()	
	 		form_h.fields['Paciente'].initial = paciente
	 		form_m.fields['Paciente'].initial = paciente
	 	return render (request, 'antecedentes/crear_antecedentes_m.html',{'form_h':form_h, 'form_m':form_m})
	 
def detalle_antecedentes_h (request, paciente_id):
	habitos = get_object_or_404(Antecedentes_habitos,Paciente=paciente_id)
	medicos = get_object_or_404(Antecedentes_medicos,Paciente=paciente_id)
	testiculares = get_object_or_404(Antecedentes_testiculares,Paciente=paciente_id)
	return render (request, 'antecedentes/detalle_antecedentes_h.html',{'habitos':habitos,'medicos':medicos,'testiculares':testiculares})

def detalle_antecedentes_m (request, paciente_id):
	habitos = get_object_or_404(Antecedentes_habitos,Paciente=paciente_id)
	medicos = get_object_or_404(Antecedentes_medicos,Paciente=paciente_id)
	#testiculares = get_object_or_404(Antecedentes_testiculares,Paciente=paciente_id)
	return render (request, 'antecedentes/detalle_antecedentes_m.html',{'habitos':habitos,'medicos':medicos})

def editar_antecedentes_h(request,paciente_id = None):
	paciente = get_object_or_404(Paciente,id=paciente_id)
	habitos = get_object_or_404(Antecedentes_habitos,Paciente=paciente_id)
	medicos = get_object_or_404(Antecedentes_medicos,Paciente=paciente_id)
	testiculares = get_object_or_404(Antecedentes_testiculares,Paciente=paciente_id)
	if request.method =='POST':
	 	form_h = habitos_form(request.POST,instance = habitos)		
	 	form_m = medicos_form(request.POST,instance = medicos)
	 	form_t = testiculares_form(request.POST,instance = testiculares)
	 	if form_h.is_valid() and form_m.is_valid() and form_t.is_valid():
	 		form_h.save()
	 		form_m.save()
	 		form_t.save()
	 		return redirect('listar_paciente')
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form_h= habitos_form(instance = habitos)	
	 	form_m = medicos_form(instance = medicos)	
	 	form_t = testiculares_form(instance = testiculares)	
	return render (request, 'antecedentes/editar_antecedentes_h.html',{'form_h':form_h, 'form_m':form_m ,'form_t':form_t})

def editar_antecedentes_m(request,paciente_id = None):
	paciente = get_object_or_404(Paciente,id=paciente_id)
	habitos = get_object_or_404(Antecedentes_habitos,Paciente=paciente_id)
	medicos = get_object_or_404(Antecedentes_medicos,Paciente=paciente_id)
	if request.method =='POST':
	 	form_h = habitos_form(request.POST,instance = habitos)		
	 	form_m = medicos_form(request.POST,instance = medicos)
	 
	 	if form_h.is_valid() and form_m.is_valid():
	 		form_h.save()
	 		form_m.save()
	 		return redirect('listar_paciente')
	else:
	 		#paciente=Paciente.objects.get(pk= 1)
	 		#paciente = paciente_id
	 	form_h= habitos_form(instance = habitos)	
	 	form_m = medicos_form(instance = medicos)		
	return render (request, 'antecedentes/editar_antecedentes_m.html',{'form_h':form_h, 'form_m':form_m })



	