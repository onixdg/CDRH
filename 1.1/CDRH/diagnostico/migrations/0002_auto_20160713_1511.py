# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-13 19:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diagnostico', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Diagnostico_uii',
            new_name='Diagnostico_iiu',
        ),
    ]
