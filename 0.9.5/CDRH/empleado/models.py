#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
#restringe campos IntergerField, PositiveIntergerField,DecimalField
from django.core.validators import MaxValueValidator, MinValueValidator
from andrologia.models import *


# Create your models here.

#-----------------------------------------CLASE PACIENTE-------------------------------------------- 

#desarrollado bajo la norma Chilena " estándares de identificación de la persona atendida"
class Empleado (models.Model):
	
	NACIONALIDAD=(
		(101,'Angola'),
		(102,'Argelia'),
		(103,'Benin'),
		(104,'Botswana'),
		(105,'Burkina Faso'), 
		(106,'Burundi'),
		(107,'Cabo Verde'), 
		(108,'Camerún'),
		(109,'Chad'),
		(110,'Comores'),
		(111,'Costa De Marfil'), 
		(112,'Djibouti'),
		(113,'Egipto'),
		(114,'Eritrea'),
		(115,'Etiopia'),
		(116,'Gabón'),
		(117,'Gambia'),
		(118,'Ghana'),
		(119,'Guinea'),
		(120,'Guinea - Bissau'), 
		(121,'Guinea Ecuatorial'), 
		(122,'Guinea Francesa'), 
		(123,'Kenia'),
		(124,'Lesotho'),
		(125,'Liberia'),
		(126,'Libia'),
		(127,'Madagascar'),
		(128,'Malawi'),
		(129,'Mali'),
		(130,'Marruecos'),
		(131,'Mauricio'), 
		(132,'Mauritania'), 
		(133,'Mayotte'),
		(134,'Mozambique'),
		(135,'Namibia'),
		(136,'Níger'),
		(137,'Nigeria'),
		(138,'Republica Centroafricana'),
		(139,'Republica Del Congo'),
		(140,'República Democrática Del Congo'),
		(141,'Reunión'),
		(142,'Ruanda'),
		(143,'Sahara Occidental'),
		(144,'Santa Elena'),
		(145,'Santo Tome Y Principe'),
		(146,'Senegal'),
		(147,'Seychelles'),
		(148,'Sierra Leona'),
		(149,'Somalia'),
		(150,'Sudáfrica'),
		(151,'Sudan'),
		(152,'Suazilandia'),
		(153,'Tanzania'),
		(154,'Togo'),
		(155,'Túnez'),
		(156,'Uganda'),
		(157,'Zaire'),
		(158,'Zambia'),
		(159,'Zimbabwe '),
		(160,'Sudan del Sur 201 Anguilla'),
		(202,'Antigua Y Barbuda'),
		(203,'Antillas Holandesas'),
		(204,'Argentina'),
		(205,'Aruba'),
		(206,'Bahamas'),
		(207,'Barbados'),
		(208,'Belice'),
		(209,'Bermudas'),
		(210,'Bolivia'),
		(211,'Brasil'),
		(212,'Canadá'),
		(213,'Chile'),
		(214,'Colombia'),
		(215,'Costa Rica'),
		(216,'Cuba'),
		(217,'Dominica'),
		(218,'Ecuador'),
		(219,'El Salvador'),
		(220,'Estados Unidos De América'),
		(221,'Granada'),
		(222,'Groenlandia'),
		(223,'Guadalupe'),
		(224,'Guatemala'),
		(225,'Guyana'),
		(226,'Guyana Francesa'),
		(227,'Haití'),
		(228,'Honduras'),
		(229,'Islas Caimán'),
		(230,'Islas Georgia Del Sur'),
		(231,'Islas Malvinas'),
		(232,'Islas Turcos Y Caicos'),
		(233,'Islas Vírgenes Americanas'),
		(234,'Islas Vírgenes Británicas'),
		(235,'Jamaica'),
		(236,'Martinica'),
		(237,'México'),
		(238,'Monserrat'),
		(239,'Nicaragua'),
		(240,'Panamá'),
		(241,'Paraguay'),
		(242,'Perú'),
		(243,'Puerto Rico'),
		(244,'Republica Dominicana'),
		(245,'San Cristóbal Y Nevis'),
		(246,'San Pedro Y Miquelón'),
		(247,'San Vicente Y Las Granadinas'),
		(248,'Santa Lucia'),
		(249,'Surina'),
		(250,'Trinidad Y Tobago'), 
		(251,'Uruguay'),
		(252,'Venezuela'),
		(301,'Afganistán'),
		(302,'Arabia Saudita'),
		(303,'Armenia'),
		(304,'Azerbaiyán'),
		(305,'Bahréin'),
		(306,'Bangladesh'),
		(307,'Bután'),
		(308,'Birmania'),
		(309,'Brunei'),
		(310,'Camboya'),
		(311,'China'),
		(312,'Emiratos Árabes Unidos'),
		(313,'Filipinas'),
		(314,'Georgia'),
		(315,'Hong Kong'),
		(316,'India'),
		(317,'Indonesia'),
		(318,'Irak'),
		(319,'Irán'),
		(320,'Israel'),
		(321,'Japón'),
		(322,'Jordania'),
		(323,'Kazajstán'),
		(324,'Kirguizistán'),
		(325,'Kuwait'),
		(326,'Laos'),
		(327,'Líbano'),
		(328,'Macao'),
		(329,'Malasia'),
		(330,'Maldivas'),
		(331,'Mongolia'),
		(332,'Nepal'),
		(333,'Omán'),
		(334,'Pakistán'),
		(335,'Palestina'),
		(336,'Qatar'),
		(337,'Republica De Corea'),
		(338,'Republica Democrática Popular De Corea'),
		(339,'Rusia'),
		(340,'Singapur'),
		(341,'Siria'),
		(342,'Sri Lanka'),
		(343,'Tailandia'),
		(344,'Taiwán'),
		(345,'Tayikistán'),
		(346,'Turkmenistán'),
		(347,'Turquía'),
		(348,'Uzbekistán'),
		(349,'Vietnam'),
		(350,'Yeme'),
		(401,'Albania'),
		(402,'Alemania'),
		(403,'Andorra'),
		(404,'Austria'),
		(405,'Bélgica'),
		(406,'Bielorrusia'),
		(407,'Bosnia - Herzegovina'),
		(408,'Bulgaria'),
		(409,'Chipre'),
		(410,'Croacia'),
		(411,'Dinamarca'),
		(412,'Eslovaquia'),
		(413,'Eslovenia'),
		(414,'España'),
		(415,'Eston'),		
		(416,'Finlandia'),
		(417,'Francia'),
		(418,'Gibraltar'),
		(419,'Grec'),		
		(420,'Holanda'),
		(421,'Hungría'),
		(422,'Irlanda423Islandia'),
		(424,'Islas Feroe'),
		(425,'Italia'),
		(426,'Letonia'),
		(427,'Liechtenstein'),
		(428,'Lituania'),
		(429,'Luxemburgo'),
		(430,'Macedonia'),
		(431,'Malta'),
		(432,'Mónaco'),
		(433,'Noruega'),
		(434,'Polonia'),
		(435,'Portugal'),
		(436,'Reino Unido'),
		(437,'Republica Checa'),
		(438,'Republica De Moldavia'),
		(439,'Rumania'),
		(440,'San Marino'),
		(441,'Suecia'),
		(442,'Suiza'),
		(443,'Ucrania'),
		(444,'Vaticano'),
		(445,'Yugoslavia'),
		(501,'Ashmore Y Cartier'),
		(502,'Australia'),
		(503,'Christmas'),
		(504,'Estados Federados De Micronesia'),
		(505,'Fiji'),
		(506,'Guam'),
		(507,'Isla Kingman Reef'),
		(508,'Isla Lord Howe'),
		(509,'Isla Norfolk'),
		(510,'Isla Palmyra'),
		(511,'Isla Wake'),
		(512,'Islas Cocos'),
		(513,'Islas Cook'),
		(514,'Islas Jarvis'),
		(515,'Islas Johnston'),
		(516,'Islas Mariana Del Norte'),
		(517,'Islas Marshall'),
		(518,'Islas Midway'),
		(519,'Islas Salomón'),
		(520,'Islas Wallis Y Futuna'),
		(521,'Kiribati'),
		(522,'Macquarie'),
		(523,'Nauru'),
		(524,'Niue'),
		(525,'Nueva Caledonia'),
		(526,'Nueva Zelanda'),
		(527,'Palau'),
		(528,'Papúa - Nueva Guinea'),
		(529,'Pitcairn'),
		(530,'Polinesia Francesa'),
		(531,'Samoa Occidental'),
		(532,'Samoa Oriental'),
		(533,'Tokelau'),
		(534,'Tonga'),
		(535,'Tuvalu'),
		(536,'Vanuatu'),

					)	
	
	
	Nombres = models.CharField(max_length = 50,blank=True,null = True)
	Primer_apellido = models.CharField("Apellido paterno",max_length = 30,blank=True,null = True)
	Segundo_apellido = models.CharField("Apellido materno",max_length = 30,blank=True,null = True)
	#ojo con el validador de rut
	RUN = models.IntegerField("RUT",blank=True,null = True )
	DV = models.CharField(max_length=1,blank=True,null = True)
	Email = models.EmailField(max_length=50,blank=True,null = True)
	Codigo = models.IntegerField("Código",blank=True,null = True)
	Telefono = models.IntegerField(blank=True,null = True)
	Nacionalidad = models.IntegerField(choices=NACIONALIDAD,blank=True,null = True)
	DI = models.CharField("Documento de identidad",max_length = 20,blank=True,null = True)
	Ocupacion = models.CharField ("Ocupación",max_length = 30,blank=True,null = True)
	#Rol

	def  __unicode__ (self):
		if (self.RUN):
			return '%s-%s: %s' % (self.RUN,self.DV, self.Nombres)
		if (self.DI):
			return '%s-%s: %s' % (self.Pasaporte,self.DV, self.Nombres)

	#sexo Y Previsión son opciones numericas según la norma de salud
	
	#ESTADO_CONYUGAL = (
		#(1,'Soltero (a)'),
		#(2,'Casado (a)',
		#(3,'Viudo (a)'),
		#(4,'Separado (a)'),
		#(5,'Conviviente (a)'),
		#(6,'Divorciado (a)),
		#(9,'Desconocido'),
		#)
			
	#PREVISION = (
		#(1,'FONASA'),
		#(2,'Isapre',
		#(3,'Sin previsión'),
		#(5,'CAPREDENA'),
		#(6,'DIPRECA),
		#(7,'Otra'),
		#(9,'Ignorado'),
		#)
	#CLASIFICACION_BENEFICIARIO_FONASA = (A,B,C,D)

	#LEYES_SOCIALES = (
		#(1,'Ley 18.490 Accidentes de transporte'),
		#(2,'LEY 16.744 Accidentes del trabajo y Enfermedades Profesionales',
		#(3,'Ley 16.744 Accidente Escolar'),
		#(4,'Ley 19.650/99 de Urgencia'),
		#(5,'PRAIS'),
		#(6,'Chile Solidario),
		#(7,'Chile Crece Conmigo'),
		#(8,'Otra Programa social'),
		#(9,'GES'),
		#)

	#NIVEL_INSTRUCCION = (
		#('01','Sala cuna'),
		#('02','Educación parvularia',
		#('03','Educación básica'),
		#('04','Educación media científica humanista'),
		#('05','Educación media técnico profesional'),
		#('06','Humanidades (sistema antiguo)'),
		#('08','Chile Crece Conmigo'),
		#('10','Ley 16.744 Accidente Escolar'),
		#('12','Educación universitaria'),
		#('13','Postgrado'),
		#('14','Educación especial'),
		#('15','Ninguno'),
		#)
	#ESTANDAR_ABREVIADO_NIVEL_INSTRUCCION = (
		#('1','Pre-básica'),
		#('2','Básica (incluye educación especial /diferencial)',
		#('3','Media'),
		#('4','Técnica de nivel superior'),
		#('5','Superior'),
		#('6','Ninguno'),
		#)
	#Estado_conyugal = models.IntergerField(dígito maximo =1,choices= ESTADO_CONYUGAL)
	#Prevision = models.Charfield("previsión",digito maximo = 1, choices = PREVISION )
	#Clasificacion_Beneficiario_FONASA = models.CharField("Clasificación beneficiario FONASA", max_length =1,null =true)
	#Leyes_sociales = models.IntergerField(dígito maximo =1,choices=LEYES_SOCIALES) 
	#def definir_clasificacion_fonada(self):
		#if self.presision == 1: deberia poner la CLASIFICACION_BENEFICIARIO_FONASA = (A,B,C,D)
	#Nivel_de_instrucción= models.Charfield("Nivel de intrucción",max_length =2,null = true ,choices = NIVEL_INSTRUCCION)
	#Estandar_abreviado_nivel_de_instrucción= model.Charfield("Estándar Abreviado Nivel de Instrucción",max_length =1,null = true ,choices = ESTANDAR_ABREVIADO_NIVEL_INSTRUCCION)
	#Ocupacion = models.Charfield ("Ocupación",max_length = 4, fuente CIUO-88)
	#Actividad_economica = models.Charfield ("Actividad Económiva",max_length = 6, fuente CIIU)
	#Nacionalidad





