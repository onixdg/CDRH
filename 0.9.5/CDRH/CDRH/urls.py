#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
	url(r'^andrologia/', include('andrologia.urls')),
    url(r'^paciente/', include('paciente.urls')),
    url(r'^registro/', include('registro.urls')),
    url(r'^hora/', include('hora.urls')),
    url(r'^antecedentes/', include('antecedentes.urls')),
    url(r'^diagnostico/', include('diagnostico.urls')),
    url(r'^informe/', include('informe.urls')),
    url(r'^empleado/', include('empleado.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home, name='home'),
]

# esto debería cambiarse en el ambiente de despliegue, codigo sólo utilizado en ambiente de desarrollo
if settings.DEBUG:
    urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)