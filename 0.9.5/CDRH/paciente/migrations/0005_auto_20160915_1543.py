# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-15 18:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('paciente', '0004_auto_20160909_1107'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paciente',
            name='DI',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
